import { Directive, ElementRef, Input, Renderer, HostListener } from '@angular/core';

@Directive({
  selector: 'ion-input[mascara]'
})
export class MascaraDirective {
    
  CPF  = "cpf";
  CNPJ = "cnpj";
  CEP  = "cep";
  TELEFONE_FIXO = "telefone-fixo";
  TELEFONE_CELULAR = "telefone-celular";
  
  TAMANHO_MAXIMO_CPF = 14;
  TAMANHO_MAXIMO_CNPJ = 18;
  TAMANHO_MAXIMO_TELEFONE_CELULAR = 15;
  
  @Input('mascara') mascara:string;

  @HostListener('keyup') onMouseLeave() {
    console.log('m', this.mascara);
    if(this.mascara == this.CPF){
      this.aplicarMascaraCpf();
    }else if(this.mascara == this.CNPJ){
      this.aplicarMascaraCnpj();
    }else if(this.mascara == this.CEP){
      this.aplicarMascaraCep();
    }else if(this.mascara == this.TELEFONE_FIXO){
      this.aplicarMascaraTelefoneFixo();
    }else if(this.mascara == this.TELEFONE_CELULAR){
      this.aplicarMascaraTelefoneCelular();
    }
  }
  
  constructor(private elementRef:ElementRef, private renderer:Renderer) {}
  
  aplicarMascaraCpf(){
    
    var cpf = this.elementRef.nativeElement.value;
    cpf = cpf.replace(/\D/g,"");
    cpf = cpf.replace(/(\d{3})(\d)/,"$1.$2");
    cpf = cpf.replace(/(\d{3})(\d)/,"$1.$2");
    cpf = cpf.replace(/(\d{3})(\d)/,"$1-$2");
    cpf = cpf.replace(/(\d{2})(\d)/,"$1$2");

    if(cpf.length > this.TAMANHO_MAXIMO_CPF){ 
      cpf = this.removerUltimoDigito(cpf);
    }
    
    this.renderer.setElementProperty(this.elementRef.nativeElement,'value',cpf);
  }
  
  aplicarMascaraCnpj(){
    var cnpj = this.elementRef.nativeElement.value;
    cnpj = cnpj.replace(/\D/g,"");
    cnpj = cnpj.replace(/(\d{2})(\d)/,"$1.$2");
    cnpj = cnpj.replace(/(\d{3})(\d)/,"$1.$2");
    cnpj = cnpj.replace(/(\d{3})(\d)/,"$1/$2");
    cnpj = cnpj.replace(/(\d{4})(\d)/,"$1-$2");
    cnpj = cnpj.replace(/(\d{3})(\d{1,2})$/,"$1-$2");
    cnpj = cnpj.replace(/(\d{2})(\d{1,2})$/,"$1$2");

    if(cnpj.length > this.TAMANHO_MAXIMO_CNPJ){ 
      cnpj = this.removerUltimoDigito(cnpj);
    }
    
    this.renderer.setElementProperty(this.elementRef.nativeElement,'value',cnpj);
  }

  aplicarMascaraCep(){
    var cep = this.elementRef.nativeElement.value;
    cep = cep.replace(/\D/g,"");
    cep = cep.replace(/(\d{2})(\d)/,"$1.$2");
    cep = cep.replace(/(\d{3})(\d)/,"$1-$2");
    cep = cep.replace(/(\d{3})(\d)/,"$1");

    this.renderer.setElementProperty(this.elementRef.nativeElement,'value',cep);
  }

  aplicarMascaraTelefoneFixo(){
    var telefoneFixo = this.elementRef.nativeElement.value;
    telefoneFixo = telefoneFixo.replace(/\D/g,"");
    telefoneFixo = telefoneFixo.replace(/(\d{2})(\d)/,"($1) $2");
    telefoneFixo = telefoneFixo.replace(/(\d{4})(\d)/,"$1-$2");
    telefoneFixo = telefoneFixo.replace(/(\d{4})(\d)/,"$1");

    this.renderer.setElementProperty(this.elementRef.nativeElement, 'value', telefoneFixo);
  }

  aplicarMascaraTelefoneCelular(){
    var telefoneCelular = this.elementRef.nativeElement.value;
    
    telefoneCelular = telefoneCelular.replace(/\D/g,""); 
    telefoneCelular = telefoneCelular.replace(/(\d{2})(\d)/,"($1) $2");
    telefoneCelular = telefoneCelular.replace(/(\d{5})(\d)/,"$1-$2");
    telefoneCelular = telefoneCelular.replace(/(\d{5})(\d)/,"$1");
    
    if(telefoneCelular.length > this.TAMANHO_MAXIMO_TELEFONE_CELULAR ) { 
      telefoneCelular = this.removerUltimoDigito(telefoneCelular);
    }
    
    this.renderer.setElementProperty(this.elementRef.nativeElement,'value',telefoneCelular);
  }

  
  removerUltimoDigito(numero){
    return numero.slice(0, -1); 
  }
}
