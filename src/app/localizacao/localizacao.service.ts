import { Injectable } from '@angular/core';
import { Platform } from '@ionic/angular';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { Localizacao } from '../model/Localizacao';
import { PermissaoService } from '../permissao/permissao.service';
import { Alerta } from '../model/Alerta';
import { AlertaService } from '../alerta/alerta.service';

@Injectable({
	providedIn: 'root'
})
export class LocalizacaoService {

	localizacao: Localizacao;
	alerta: Alerta;
	constructor(
		private geolocation: Geolocation,
		private platform: Platform,
		private permissaoService: PermissaoService,
		private alertaService: AlertaService
	) {

	}

	localizar() {
		return new Promise((resolve, reject) => {

			if (this.platform.is('ios') || this.platform.is('android')) {
				this.permissaoService.solicitarPermissaoParaLocalizacao().then(permissao => {

					if (permissao == 'DENIED_ALWAYS') {

						this.alerta = new Alerta();
						this.alerta.mensagem = ' A permissão para acesso ao serviço de localização não foi concedido. Deseja permitir ? ';

						this.alertaService.apresentarAlertaDeAcao(this.alerta, () => {
							this.permissaoService.abrirConfiguracoesDoAPP().then(retorno => {
								this.obterLocalizacao().then((localizacao: Localizacao) => {
									resolve(localizacao);
								}).catch(() => {
									reject();
								})
							})
						})

					} else {

						this.obterLocalizacao().then((localizacao: Localizacao) => {
							resolve(localizacao);
						}).catch(() => {
							reject();
						})
					}
				})

			} else {
				reject();
			}
		});

	}


	obterLocalizacao() {
		return new Promise((resolve, reject) => {

			this.permissaoService.localizacaoAtiva().then(ativa => {

				if (ativa) {

					var options = {
						enableHighAccuracy: true,
						timeout: 5000,
						maximumAge: 0
					};
					this.geolocation.getCurrentPosition(options)
						.then((resp) => {
							this.localizacao = new Localizacao(resp.coords.latitude, resp.coords.longitude);
							resolve(this.localizacao);
						}).catch((error) => {
							reject();
						});

				} else {
					this.alerta = new Alerta();
					this.alerta.mensagem = 'Por favor ative o serviço de localização';
					this.alertaService.apresentarAlertaDeAviso(this.alerta, () => {
						this.permissaoService.ativarLocalizacao();
					});
				}

			}).catch(error => { });
		})

	}


	acompanharLocalizacao() {
		return this.geolocation.watchPosition();
	}
}
