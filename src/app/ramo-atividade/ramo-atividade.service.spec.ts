import { TestBed } from '@angular/core/testing';

import { RamoAtividadeService } from './ramo-atividade.service';

describe('RamoAtividadeService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RamoAtividadeService = TestBed.get(RamoAtividadeService);
    expect(service).toBeTruthy();
  });
});
