import { Injectable } from '@angular/core';
import { RamoAtividade } from '../model/RamoAtividade';
import { Ambiente } from '../model/Ambiente';

import { AmbienteService } from '../ambiente/ambiente.service';

import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RamoAtividadeService {

  ramoAtividades: Array<RamoAtividade> = new Array<RamoAtividade>();
  ambiente: Ambiente = new Ambiente();

  constructor(private http: HttpClient, private ambienteService: AmbienteService) {  }

  buscar() {
    this.ambiente = this.ambienteService.getAmbiente();
    return new Promise( (resolve, reject) => {

      if (this.ramoAtividades.length === 0) {
        this.http.get(this.ambiente.url + 'ramoatv/mobile').pipe(
          map(ramoAtividades =>
          this.ramoAtividades = ramoAtividades as Array<RamoAtividade>
        )).subscribe((ramoAtividades: Array<RamoAtividade>) => {

          resolve(ramoAtividades);
        });

      } else {
        resolve(this.ramoAtividades);
      }
    });
  }
}
