import { Component, OnInit } from '@angular/core';
import { Orgao, LinkVOs } from '../model/Orgao';
import { NavController } from '@ionic/angular';
import { OrgaoService } from '../orgao/orgao.service';
import { MenuSuperiorService } from '../menu-superior/menu-superior.service';
import { AguardeService } from '../aguarde/aguarde.service';
import { AbrirLinkService } from '../abrir-link/abrir-link.service';
import { TelaInicialService } from '../tela-inicial/tela-inicial.service';
import { SolicitacaoService } from '../solicitacao/solicitacao.service';
import { Servico } from '../model/Servico';

@Component({
  selector: 'app-solicitacoes',
  templateUrl: './solicitacoes.page.html',
  styleUrls: ['./solicitacoes.page.scss'],
})
export class SolicitacoesPage implements OnInit {

  orgao: Orgao = new Orgao();
  constructor(
    private navCtrl: NavController,
    private orgaoService: OrgaoService,
    private menuSuperiorService: MenuSuperiorService,
    private aguardarService: AguardeService,
    private solicitacaoService: SolicitacaoService,
    private abrirLinkService: AbrirLinkService,
    private telaInicialService: TelaInicialService
  ) {
    this.orgao = this.orgaoService.getOrgao();

    this.obterServicos();
    this.obterLinks();
  }

  ngOnInit() {}
  obterServicos() {
    this.aguardarService.processar('buscando serviços..').then(() => {
      this.orgaoService.getOrgaoRequest(this.orgao.codorg).then((value: Array<Servico>) => {
        this.orgao.servicos = value;
        this.aguardarService.encerrar();
      }).catch(() => this.aguardarService.encerrar());
    }).catch(() => this.aguardarService.encerrar());
  }
  obterLinks() {
    this.orgaoService.getLinksOrgaoRequest(this.orgao.codorg).then((value: Array<LinkVOs>) => {
      this.orgao.linkVos = this.orgao.linkVos.sort((a, b) => {
        if (a.ordlink <= b.ordlink)
          return 1;
        else
          return -1;
      });
      this.aguardarService.encerrar();
    }).catch((error) => console.log(error));
  }

  ionViewWillEnter() {
    this.menuSuperiorService.setTitle(this.orgao.botaoorg.toUpperCase(), '');
    this.menuSuperiorService.showBackButton(true);
  }

  ngOnDestroy() {
    this.telaInicialService.scrollIntoView();
  }

  abrirLink(link) {
    this.abrirLinkService.abrir(link);
  }

  solicitacao(servico) {
    this.solicitacaoService.setServico(servico);
    this.navCtrl.navigateForward('solicitacao');
  }
  AbrirTempoAtendimento() {
    this.navCtrl.navigateForward('tempo-atendimento');
  }
}
