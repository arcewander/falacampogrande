import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { SolicitacoesPage } from './solicitacoes.page';
import { MenuSuperiorPageModule } from '../menu-superior/menu-superior.module';

const routes: Routes = [
  {
    path: '',
    component: SolicitacoesPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    MenuSuperiorPageModule
  ],
  declarations: [SolicitacoesPage]
})
export class SolicitacoesPageModule {}
