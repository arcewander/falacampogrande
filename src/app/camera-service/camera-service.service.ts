import { Injectable } from '@angular/core';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';

import { Platform } from '@ionic/angular';
import { Solicitacao } from '../model/Solicitacao';
import { AguardeService } from '../aguarde/aguarde.service';
import { ArquivoService } from '../arquivo-service/arquivo-service.service';

@Injectable({
  providedIn: 'root'
})
export class CameraService {


  private solicitacao: Solicitacao = new Solicitacao();

  constructor(public platform: Platform,
              private camera: Camera,
              private aguardeService: AguardeService,
              private arquivoService: ArquivoService) { }

  capturarFoto() {
    return new Promise((resolve, reject) => {
      const options: CameraOptions = {
        quality: 100,
        destinationType: this.camera.DestinationType.FILE_URI,
        encodingType: this.camera.EncodingType.JPEG,
        mediaType: this.camera.MediaType.PICTURE
      };

      this.camera.getPicture(options).then((imageData) => {
        this.aguardeService.processar('Anexando o Arquivo');
        this.arquivoService.carregarArquivo(imageData)
          .then(arquivo => {
            this.aguardeService.encerrar();
            resolve(arquivo);
          })
          .catch(error => {
            this.aguardeService.encerrar();
            reject(error);
          });

      }, (err) => {
        console.log(err);
        this.aguardeService.encerrar();
      });
    });

  }

  selecionarImagem() {
    return new Promise((resolve, reject) => {
      if (this.platform.is('ios')) {
        this.selecionarImagemIOS()
          .then(arquivo => { resolve(arquivo); })
          .catch(error => reject(error));

      } else {
        this.aguardeService.processar('Anexando arquivo..').then(() => {
          this.arquivoService.selecionarArquivoAndroid()
            .then(arquivo => {
              this.aguardeService.encerrar();
              resolve(arquivo);
            })
            .catch(error => {
              this.aguardeService.encerrar();
              reject(error);
            });
        });
      }
    });
  }


  selecionarImagemIOS() {
    return new Promise((resolve, reject) => {

      const options: CameraOptions = {
        quality: 100,
        destinationType: this.camera.DestinationType.FILE_URI,
        encodingType: this.camera.EncodingType.JPEG,
        mediaType: this.camera.MediaType.PICTURE,
        sourceType: this.camera.PictureSourceType.PHOTOLIBRARY
      };

      this.camera.getPicture(options)
        .then((imageData) => {

          this.arquivoService.carregarArquivo(imageData)
            .then(arquivo => { resolve(arquivo); })
            .catch(error => reject(error));

        }).catch(err => console.log(err));

    });
  }



}
