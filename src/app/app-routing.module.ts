import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  { path: 'tela-inicial', loadChildren: './tela-inicial/tela-inicial.module#TelaInicialPageModule' },
  { path: 'cadastrar-usuario', loadChildren: './cadastrar-usuario/cadastrar-usuario.module#CadastrarUsuarioPageModule' },
  { path: 'login', loadChildren: './login/login.module#LoginPageModule' },
  { path: 'redefinir-senha', loadChildren: './redefinir-senha/redefinir-senha.module#RedefinirSenhaPageModule' },
  { path: 'perfil', loadChildren: './perfil/perfil.module#PerfilPageModule' },
  { path: 'configuracoes', loadChildren: './configuracoes/configuracoes.module#ConfiguracoesPageModule' },
  { path: 'sobre', loadChildren: './sobre/sobre.module#SobrePageModule' },
  { path: 'solicitacoes', loadChildren: './solicitacoes/solicitacoes.module#SolicitacoesPageModule' },
  { path: 'solicitacao', loadChildren: './solicitacao/solicitacao.module#SolicitacaoPageModule' },
  { path: 'ramo-atividade-modal', loadChildren: './ramo-atividade-modal/ramo-atividade-modal.module#RamoAtividadeModalPageModule' },
  { path: 'termo', loadChildren: './termo/termo.module#TermoPageModule' },
  { path: 'notificacao', loadChildren: './notificacao/notificacao.module#NotificacaoPageModule' },
  { path: 'logout', loadChildren: './logout/logout.module#LogoutPageModule' },
  { path: 'login', loadChildren: './login/login.module#LoginPageModule' },
  { path: 'tempo-atendimento', loadChildren: './tempo-atendimento/tempo-atendimento.module#TempoAtendimentoPageModule' },
  { path: 'alterar-senha', loadChildren: './alterar-senha/alterar-senha.module#AlterarSenhaPageModule' },
  { path: 'termo-uso-app', loadChildren: './termo-uso-app/termo-uso-app.module#TermoUsoAppPageModule' },
  { path: 'obrigado', loadChildren: './obrigado/obrigado.module#ObrigadoPageModule' },
  { path: 'minhas-solicitacoes', loadChildren: './minhas-solicitacoes/minhas-solicitacoes.module#MinhasSolicitacoesPageModule' },
  { path: 'legenda', loadChildren: './minhas-solicitacoes/legenda/legenda.module#LegendaPageModule' },
  { path: 'contato', loadChildren: './procon/contato/contato.module#ContatoPageModule' },
  { path: 'ouvidoria', loadChildren: './ouvidoria/ouvidoria.module#OuvidoriaPageModule' },
  { path: 'endereco-modal', loadChildren: './procon/endereco-modal/endereco-modal.module#EnderecoModalPageModule' },
  { path: 'informacoes-consumidor', loadChildren: './procon/informacoes-consumidor/informacoes-consumidor.module#InformacoesConsumidorPageModule' },
  { path: 'orientacao', loadChildren: './procon/orientacao/orientacao.module#OrientacaoPageModule' },
  { path: 'procon/minhas-solicitacoes', loadChildren: './procon/minhas-solicitacoes/minhas-solicitacoes.module#MinhasSolicitacoesPageModule' },
  { path: 'procon/tela-inicial-procon', loadChildren: './procon/tela-inicial/tela-inicial.module#TelaInicialPageModule' },
  { path: 'denuncia', loadChildren: './procon/denuncia/denuncia.module#DenunciaPageModule' },
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
