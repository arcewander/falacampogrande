import { Injectable } from '@angular/core';
import { Usuario } from '../model/Usuario';
import { Notificacao } from '../model/Notificacao';
import { Ambiente } from '../model/Ambiente';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { SQLiteObject } from '@ionic-native/sqlite/ngx';
import { timeout } from 'q';
import { DatabaseService } from '../database/database.service';
import { AmbienteService } from '../ambiente/ambiente.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  usuario: Usuario = new Usuario();
  notificacao: Notificacao = new Notificacao();
  params: any[];
  ambiente: Ambiente = new Ambiente();
  header: HttpHeaders;

  constructor(
    private http: HttpClient,
    private databaseService: DatabaseService,
    private ambienteService: AmbienteService
  ) {
    this.header = this.getHeader();
    this.ambienteAtual();
  }

  login(usuario: Usuario) {

    this.ambiente = this.ambienteService.getAmbiente();
    //this.ambiente.url = 'http://fala.campogrande.ms.gov.br/gesol/api/';
    console.info(usuario);
    const senha = usuario.senuso;
    return this.http.post(this.ambiente.url + 'acesso/login',
    usuario, { headers: this.header }).pipe(map(data  => {
          this.usuario = <Usuario> data;
          this.usuario.senuso = senha;
          return this.usuario;
        }));

  }

  setUsuario(usuario: Usuario) {
    this.usuario = usuario;
  }

  getUsuario() {
    return this.usuario;
  }

  async salvar(usuario: Usuario) {

    await this.databaseService.abrirConexao().then((db: SQLiteObject) => {
      db.executeSql('INSERT INTO usuario (coduso, cpfuso ,senuso) VALUES(?,?,?) ', [usuario.coduso, usuario.cpfuso, usuario.senuso])
        .then((res) => {console.log('inserido');
                        alert(JSON.stringify(res));
        }).catch(e => {console.log('erro', e);
                       alert(JSON.stringify(e));
        });
    }, (error) => {
      console.log('ERROR: ', error);
      alert(JSON.stringify(error));
    });
  }

  async buscarUsuario(callback) {

    await this.databaseService.abrirConexao().then((db: SQLiteObject) => {
      db.executeSql('SELECT * FROM usuario', [])
        .then(usuario => {
          console.log('achou user: ', usuario.rows.item(0));
          if (usuario.rows.length > 0) {
            this.usuario = usuario.rows.item(0);
            callback(this.usuario);
          }
        }).catch(error => console.log('error na busca de usuario', error));
    }).catch(erro => console.log('Não foi possivel abrir conexão com o SQlite', erro));
  }

  remove(id: any) {
    return this.databaseService.abrirConexao()
      .then((db: SQLiteObject) => {

        return db.executeSql('DELETE FROM usuario')
          .catch((e) => console.error(e));
      })
      .catch((e) => console.error(e));
  }

  ambienteAtual() {
    this.ambienteService.ambienteAtual((ambiente) => {
      this.ambiente = ambiente;
    });
  }

  getHeader() {
    return new HttpHeaders({
      'Content-Type': 'application/json;',
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, OPTIONS'
    });
  }
}
