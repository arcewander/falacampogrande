import { Component, OnInit } from '@angular/core';
import { Alerta } from '../model/Alerta';
import { Usuario } from '../model/Usuario';
import { Router } from '@angular/router';
import { NavController, MenuController } from '@ionic/angular';
import { Localizacao } from '../model/Localizacao';
import { LoginService } from './login.service';
import { AlertaService } from '../alerta/alerta.service';
import { AguardeService } from '../aguarde/aguarde.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  usuario: Usuario;
  alerta: Alerta;
  localizacao: Localizacao;

  passwordType = 'password';
  passwordShown = false;

  constructor(private router: Router,
              public navCtrl: NavController,
              private loginService: LoginService,
              private aguardeService: AguardeService,
              private alertaService: AlertaService,
              private menuController: MenuController) {

    this.menuController.enable(false);
    this.usuario = new Usuario();
    this.usuario = loginService.getUsuario();
  }

  public togglePassword() {
    if (this.passwordShown) {
      this.passwordShown = false;
      this.passwordType = 'password';
    } else {
      this.passwordShown = true;
      this.passwordType = 'text';
    }
  }

  ngOnInit() {
    setTimeout(() => {
    //Only DEV
     //this.usuario.cpfuso = '014.610.061-12';
     //this.usuario.senuso = '01461006112';
    // this.usuario.senuso = '88979141017';
    }, 1000);
  } 

  entrar() {
    let valido = this.validar();
    let senha = this.usuario.senuso;

    if (valido === true) {
      if (this.usuario.cpfuso === '999.999.999-99' && senha === 'mercurio') {
        this.configuracoes();
      } else {
        this.aguardeService.processar('').then(() => {

          this.loginService.login(this.usuario).subscribe((usuario: Usuario) => {

            let user = usuario;
            //alert(JSON.stringify(usuario));
            // this.usuario = usuario;
            // this.usuario.senuso = senha;
            // this.usuario.cpfuso = usuario.cpfuso;
            this.loginService.setUsuario(user);
            this.loginService.salvar(user);

            this.aguardeService.encerrar();
            this.menuController.enable(true);
            if (user !== undefined) {
              this.irTelaPrincipal();
            }
          },
            error => {
              this.navCtrl.navigateBack('login');
              alert(JSON.stringify(error));
              this.aguardeService.encerrar();
              this.alerta =  error.error as Alerta;
              this.alertaService.apresentarAlertaDeAviso(this.alerta, () => { });

            });
        });
      }

    }
  }

  cadastrar() {
    this.router.navigate(['/cadastrar-usuario']);
  }

  configuracoes() {
    this.router.navigate(['/configuracoes']);
    this.limparCpfSenha();
  }

  async irTelaPrincipal() {
    await this.navCtrl.navigateRoot('/tela-inicial');
    //this.limparCpfSenha();
  }

  limparCpfSenha() {
    setTimeout(() => {
      this.usuario.cpfuso = '';
      this.usuario.senuso = '';
    }, 2000);

  }

  redefinirSenha() {
    this.navCtrl.navigateForward('/redefinir-senha');
  }

  solicitacoes() {
    this.navCtrl.navigateForward('solicitacoes');
  }

  validar() {
    if (this.verificarCampoEmBranco(this.usuario.cpfuso) === '' || this.usuario.cpfuso === undefined) {
      this.apresentarAlerta('Atenção', 'Informe o CPF.');
      return false;
    }
    if (this.verificarCampoEmBranco(this.usuario.senuso) === '' || this.usuario.senuso === undefined) {
      this.apresentarAlerta('Atenção', 'Informe a Senha.');
      return false;
    }
    return true;
  }

  apresentarAlerta(titulo, mensagem) {
    this.alerta = new Alerta();
    this.alerta.titulo = titulo;
    this.alerta.mensagem = mensagem;
    this.alertaService.apresentarAlertaDeAviso(this.alerta, () => { })
  }

  hasProp(obj, prop) {
    return Object.prototype.hasOwnProperty.call(obj, prop);
  }

  verificarCampoEmBranco(input: string | string[]) {
    if (typeof input === 'string') {
      return input.trim();
    } else {
      return input;
    }
  }

}
