import { TestBed } from '@angular/core/testing';

import { TermoService } from './termo.service';

describe('TermoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TermoService = TestBed.get(TermoService);
    expect(service).toBeTruthy();
  });
});
