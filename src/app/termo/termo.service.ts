import { Injectable } from '@angular/core';
import { Termo } from '../model/Termo';
import { Ambiente } from '../model/Ambiente';
import { HttpClient } from '@angular/common/http';
import { AmbienteService } from '../ambiente/ambiente.service';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TermoService {

  termo: Termo = new Termo();
  ambiente: Ambiente = new Ambiente();

  constructor(private http: HttpClient, private ambienteService: AmbienteService) { }

   buscar(codtpt?: Number) {
    this.ambiente = this.ambienteService.getAmbiente();
  	 return this.http.get(this.ambiente.url + 'termo/codtpt/' + codtpt).pipe(
          map((termo: Termo) => {
            return this.termo = termo;
          } ));
  }
}
