import { Component, OnInit } from '@angular/core';
import { Termo } from '../model/Termo';
import { ModalController, NavController, NavParams, Platform } from '@ionic/angular';
import { ModalService } from '../modal-service/modal.service';
import { AguardeService } from '../aguarde/aguarde.service';
import { AlertaService } from '../alerta/alerta.service';
import { TermoService } from './termo.service';

@Component({
  selector: 'app-termo',
  templateUrl: './termo.page.html',
  styleUrls: ['./termo.page.scss'],
})
export class TermoPage implements OnInit {

  termo: Termo = new Termo();
	modal: ModalController;
	tela: string;
	constructor(private modalController: ModalController,
		private navCtrl: NavController,
		public navParams: NavParams,
		private platform: Platform,
		private modalService: ModalService,
		private aguardeService: AguardeService,
		private alertaService: AlertaService,
		private termoService: TermoService) {
		this.aguardeService.processar("").then(() => {
			this.termo.codtpt = navParams.get('codtpt');
			this.aguardeService.encerrar();
			this.buscarTermo(this.termo.codtpt);
		});
	}

	ngOnInit() { }

	buscarTermo(codtpt: Number) {
		this.termoService.buscar(codtpt).subscribe((termo: Termo) => {
		this.termo = termo;
			this.setTela(this.termo.codtpt);
		})
	}

	irPara() {
		this.modal.dismiss();
		this.modalService.remover();
		this.navCtrl.navigateForward(this.tela);
	}

	setTela(codtpt: Number) {
		if (codtpt == 5) {
			this.tela = "denuncia";
		}
		if (codtpt == 1) {
			this.tela = "informacoes-consumidor";
		}
		if (codtpt == 2) {
			this.tela = "orientacao";
		}
	}

	fecharModal() {
		this.modal.dismiss();
	}


}
