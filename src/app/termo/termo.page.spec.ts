import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TermoPage } from './termo.page';

describe('TermoPage', () => {
  let component: TermoPage;
  let fixture: ComponentFixture<TermoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TermoPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TermoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
