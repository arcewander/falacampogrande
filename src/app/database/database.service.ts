import { Injectable } from '@angular/core';
import { Notificacao } from '../model/Notificacao';
import { SQLiteObject, SQLite } from '@ionic-native/sqlite/ngx';
import { Platform } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class DatabaseService {

  database: SQLiteObject = null;
  constructor(private sqlite: SQLite,
              private platform: Platform) {}

  abrirConexao() {
    return new Promise<SQLiteObject>((resolve, reject) => {
      if( this.platform.is('ios') || this.platform.is('android') ) {
        resolve(
          this.sqlite.create({
            name: 'falacg.db',
            location: 'default'
          })
        );
      } else {
        reject(console.error('error')
        );
      }
    });

  }

  setDatabase(db: SQLiteObject) {
    if (this.database == null || this.database === undefined) {
      this.database = db;
      console.log('db', JSON.stringify(db));
      console.log('aaaaaaaaa', JSON.stringify(this.database));
    }
  }

  async criarTabelas() {
    const db = await this.abrirConexao();
    await this.criarTabelaUsuario(db);
    await this.criarTabelaAmbiente(db);
    await this.criarTabelaNotificacao(db);
  }

   async criarTabelaUsuario(db: SQLiteObject) {
       await db.executeSql(
          'CREATE TABLE IF NOT EXISTS usuario ( coduso INTEGER, ' +
          'cpfuso VARCHAR(32), senuso VARCHAR(60) ); '
        , []).then((success) => {console.log('tabela usuario criada', success);
        }).catch((error) => {console.error('tabela usuario criada', error);
        });
  }

  async criarTabelaNotificacao(db: SQLiteObject) {

      console.log('Criando Tabela Notificação....');
      await db.executeSql(
          'CREATE TABLE IF NOT EXISTS notificacao (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, codigo_notificacao INTEGER, atendimento VARCHAR(32) ,'
          + ' descricao VARCHAR(255), titulo VARCHAR(250), mensagem TEXT, sitnot INTEGER, dtsnot DATE, dtlnot DATE, dtrnot DATE ); '
        ,[]).then((success) => {console.log('tabela notificacao criada', success);
        }).catch((error) => {console.error('tabela notificacao nao criada', error);
        });
  }

  async criarTabelaAmbiente(db: SQLiteObject) {
      try {
         const success = await db.executeSql(
           'CREATE TABLE IF NOT EXISTS ambiente (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL , url VARCHAR(255), nome VARCHAR(100) , ativo INTEGER ); '
           , []);
         await this.insertAmbientes(db);
       } catch (error) {
         console.error('tabela ambiente nao criada', JSON.stringify(error));
       }
  }

   async insertAmbientes(db: SQLiteObject) {
    const sql = 'INSERT INTO ambiente (url, nome, ativo) VALUES(?,?,?);';
    const ambientes = [{ 'url': 'http://172.17.0.53:8060/gesol/api/', 'nome': 'HOMOLOGAÇÃO', 'ativo': 0 },
    { 'url': 'http://fala.campogrande.ms.gov.br/gesol/api/', 'nome':' PRODUÇÃO', 'ativo': 0 },
    { 'url': 'http://10.4.1.156:8080/gesol/api/', 'nome':'LOCAL', 'ativo': 1 }  
  ]

    for(let i=0; i<ambientes.length; i++ ) {
          await this.verificarAmbientes(db, ambientes[i]).then((encontrado)=>{
          if(encontrado==false){
              db.executeSql(sql,  [ ambientes[i].url, ambientes[i].nome, ambientes[i].ativo ]
              );
            }
          });
        }
  }

  async verificarAmbientes(db: SQLiteObject, ambiente) {
    const sql = 'SELECT * FROM  ambiente WHERE url= ? AND nome = ? ;';
    try {
      const results = await db.executeSql(sql, [ambiente.url, ambiente.nome]);
      if (results.rows.length > 0) {
        return true;
      } else {
        return false;
      }
    } catch (error) {
      console.error('falha na inserção', error);
    }

  }

  async update(notificacao: Notificacao) {
    const data = [ notificacao.sitnot, notificacao.dtsnot, notificacao.dtlnot, notificacao.dtrnot,
             notificacao.codigo_notificacao];
    const sql = 'UPDATE notificacao SET sitnot = ?, dtsnot = ?, dtlnot = ?, dtrnot = ? WHERE codigo_notificacao = ? ';

    console.log('update? ', notificacao);
    const db = await this.abrirConexao();
    db.executeSql(sql, data).then(success => {
      console.log(success);
      return success;
    }).catch(error => {
      console.log(JSON.stringify(error));
      return error;
    });
  }

   async delete(notificacao: Notificacao) {
      const data = [ notificacao.codigo_notificacao];
      const sql = 'DELETE FROM notificacao WHERE codigo_notificacao = ? ';

      console.log('DELETE? ', notificacao);
      const db = await this.abrirConexao();
      db.executeSql(sql, data).then(success => {
       console.log(success);
       return success;
      }).catch(error => {
       console.log(JSON.stringify(error));
       return error;
      });
  }

  reset() {
    this.abrirConexao().then((db: SQLiteObject) => {
      this.insertAmbientes(db);
    }).catch((error) => {});
  }

}
