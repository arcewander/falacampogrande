import { Injectable } from '@angular/core';
import { Orgao, LinkVOs } from '../model/Orgao';
import { Ambiente } from '../model/Ambiente';
import { AmbienteService } from '../ambiente/ambiente.service';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Servico } from '../model/Servico';

@Injectable({
  providedIn: 'root'
})
export class OrgaoService {

  orgaos: Array<Orgao> = new Array<Orgao>();
  orgao: Orgao = new Orgao();
  ambiente: Ambiente = new Ambiente();

  constructor(private http: HttpClient, private ambienteService: AmbienteService) {

  }

  buscar() {
    this.ambiente = this.ambienteService.getAmbiente();
    return new Promise((resolve, reject) => {
      if (this.orgaos.length === 0) {
        //this.ambiente.url = 'http://fala.campogrande.ms.gov.br/gesol/api/';
        this.http.get(this.ambiente.url + 'orgaos/orgaoServicos').pipe(
          map(orgaos =>
            this.orgaos = orgaos as Array<Orgao>
          )).subscribe((orgaos: Array<Orgao>) => {
            resolve(orgaos);
          });
      } else {
        resolve(this.orgaos);
      }
    });
  }

  setOrgao(orgao) {
    this.orgao = orgao;
  }

  getOrgao() {
    return this.orgao;
  }

  getOrgaos() {
    return this.orgaos;
  }

  getOrgaoRequest(orgaoid) {
    //this.ambiente.url="http://fala.campogrande.ms.gov.br/gesol/api/";
    return new Promise((resolve, reject) => {
      this.http.get(this.ambiente.url + 'servicos/servicos_orgao/' + orgaoid).subscribe((json) => {
        const servicos = json as Array<Servico>;
        resolve(servicos);
      });
    });
  }

  getLinksOrgaoRequest(orgaoid) {
    return new Promise((resolve, reject) => {
      this.http.get(this.ambiente.url + 'links/' + orgaoid).subscribe((json) => {
        const links = json as Array<LinkVOs>;
        resolve(links);
      });
    });
  }
}
