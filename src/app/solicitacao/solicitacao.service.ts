import { Injectable } from '@angular/core';
import { Servico } from '../model/Servico';
import { Ambiente } from '../model/Ambiente';
import { AmbienteService } from '../ambiente/ambiente.service';
import { Solicitacao } from '../model/Solicitacao';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SolicitacaoService {

  servico: Servico;
  ambiente: Ambiente = new Ambiente();

  constructor(private http: HttpClient, private ambienteService: AmbienteService) { }

  enviar(solicitacao: Solicitacao) {
    this.ambiente = this.ambienteService.getAmbiente();
    console.log(JSON.stringify(solicitacao));
    return this.http.post(this.ambiente.url + 'solicitacao/solicitar',
      solicitacao,
      { headers: { 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*' } }
    );
  }

  setServico(servico) {
    this.servico = servico;
  }

  getServico() {
    return this.servico;
  }
}
