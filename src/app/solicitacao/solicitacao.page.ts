import { LocalizacaoService } from './../localizacao/localizacao.service';
import { Localizacao } from './../model/Localizacao';
import { LoginService } from './../login/login.service';
import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { Servico } from '../model/Servico';
import { SolicitacaoService } from './solicitacao.service';
import { OrgaoService } from '../orgao/orgao.service';
import { Orgao } from '../model/Orgao';
import { Solicitacao } from '../model/Solicitacao';
import { Arquivo } from '../model/Arquivo';
import { AlertaService } from '../alerta/alerta.service';
import { Alerta } from '../model/Alerta';
import { AguardeService } from '../aguarde/aguarde.service';
import { Router } from '@angular/router';
import { NavController, ModalController } from '@ionic/angular';
import { ModalService } from '../modal-service/modal.service';
import { ObrigadoPage } from '../obrigado/obrigado.page';
import { Usuario } from '../model/Usuario';
import { MenuSuperiorService } from '../menu-superior/menu-superior.service';
import { CameraService } from '../camera-service/camera-service.service';

@Component({
  selector: 'app-solicitacao',
  templateUrl: './solicitacao.page.html',
  styleUrls: ['./solicitacao.page.scss'],
})
export class SolicitacaoPage implements OnInit {
  desc: String;
  solicitacao: Solicitacao = new Solicitacao();
  orgao: Orgao = new Orgao();
  servico: Servico = new Servico();
  localizacao: Localizacao;
  alerta = new Alerta();
  arquivo: Arquivo;
  usuario: Usuario;
  arquivos: Array<Arquivo> = new Array<Arquivo>();

  disabled: boolean = false;
  modal: HTMLIonModalElement;

  constructor(
    private router: Router,
    private navCrtl: NavController,
    public modalController: ModalController,
    private orgaoService: OrgaoService,
    private solicitacaoService: SolicitacaoService,
    private loginService: LoginService,
    private localizacaoService: LocalizacaoService,
    private alertaService: AlertaService,
    private modalService: ModalService,
    private cameraService: CameraService,
    private aguardeService: AguardeService,
    private menuSuperiorService: MenuSuperiorService
  ) {
    this.orgao = this.orgaoService.getOrgao();
    this.usuario = this.loginService.getUsuario();
    this.servico = this.solicitacaoService.getServico();
    this.desc = this.servico.destps;
    this.menuSuperiorService.setTitle(this.orgao.botaoorg.toUpperCase());
    this.menuSuperiorService.showBackButton(true);
  }

  ngOnInit() { }

  selecionarServico(nome) {

    setTimeout(() => {
      const checkboxs = <HTMLCollection> document.getElementsByClassName('checkbox');
      let servicoSelecioando = (<HTMLInputElement>checkboxs.namedItem(nome));
      for (let i = 0; i < checkboxs.length; i++) {
        let checkbox = <HTMLInputElement>checkboxs.item(i);
        checkbox.checked = false;
      }

      servicoSelecioando.checked = true;
      this.servico.tiposs = parseInt(servicoSelecioando.value);
      this.solicitacao.ossVO = this.servico;
      if (this.servicoSelecionado()) {
        this.desativarBotao();
      } else {
        this.ativaBotao();
      }

    }, 100);

  }

  servicoSelecionado() {
    return this.servico.tiposs === 0;
  }

  desativarBotao() {
    this.disabled = true;
  }

  ativaBotao() {
    this.disabled = false;
  }

  removerAnexo(item: Arquivo) {
    for (let i = 0; i < this.arquivos.length; i++) {
      if (this.arquivos[i] === item) {
        this.arquivos.splice(i, 1);
      }
    }

    if (this.servicoSelecionado() && this.arquivos.length == 0) {
      this.desativarBotao();
    } else {
      this.ativaBotao();
    }
  }

  salvar() {

    let valido = this.validarSolicitacao();
    if (valido === true) {
      this.aguardeService.processar('Enviando Solicitação').then(() => {

        this.solicitacao.coduso = this.usuario.coduso;
        if (this.servico.tiposs == 0) {
          this.solicitacao.txtsol = this.arquivo.arquivoUrl;
        }

        this.solicitacaoService.enviar(this.solicitacao).subscribe((alerta: Alerta) => {
          this.aguardeService.encerrar();
          this.alerta = alerta;
          this.alertaService.apresentarAlertaDeSucesso(this.alerta, () => {
            this.apresentarObrigado();
          });
          this.servico.obsoss = '';
          this.solicitacao = new Solicitacao();
        }
          , error => {
            this.aguardeService.encerrar();
            this.alerta = new Alerta();
            this.alerta = error.error;
            this.alertaService.apresentarAlertaDeAviso(this.alerta, () => { });

          });

      });
    }
  }

  capturarFoto() {
    this.cameraService.capturarFoto().then((arquivo: Arquivo) => {
      this.aguardeService.processar('Anexando o Arquivo');

      this.vincularArquivos(arquivo);
      setTimeout(() => {
        this.aguardeService.encerrar();
      }, 1000);
      this.localizacaoService.localizar()
        .then((localizacao: Localizacao) => {
          // APOS OS TESTE OBTER USUARIO DO LOGIN SERVICE
          this.solicitacao.latsol = localizacao.latitude;
          this.solicitacao.lonsol = localizacao.longitude;
        },
          error => {
            this.alerta = new Alerta();
            this.alerta.mensagem = 'Não foi possível capturar a sua localização atual!';
            this.alertaService.apresentarAlertaDeAviso(this.alerta, () => { });
          });
    }
      , error => {
        this.alerta = new Alerta();
        this.alerta = error.error;
        this.alertaService.apresentarAlertaDeAviso(this.alerta, () => { });
      });
  }

  async vincularArquivos(arquivo) {
    this.arquivo = arquivo;
    this.arquivos.push(arquivo);

    if (this.arquivos.length >= 1) {
      this.ativaBotao();
    }
  }

  validarSolicitacao() {
    if (this.servico.codtps == null || this.servico.codtps === undefined) {
      this.apresentarMensageDeValidacao('', 'Não foi possível identificar o serviço selecionado!');
      return false;
    } else if (this.servico.tiposs == null || this.servico.tiposs === undefined) {
      this.apresentarMensageDeValidacao('Atenção', 'Por favor, selecione uma das opções: Denúncia, Elogio, Reclamação ou Serviço!');
      return false;
    } else if (this.servico.tiposs === 0 && this.arquivos.length <= 0) {
      this.apresentarMensageDeValidacao('', 'Por favor, tire uma foto para que possamos atender melhor a sua solicitação!');
      return false;
    }  else if (this.servico.obsoss == null || this.servico.obsoss == undefined || this.servico.obsoss.trim() == '') {
      this.apresentarMensageDeValidacao('', 'Por favor, preencha os detalhes de sua solicitação!');
      return false;
    } else {
      return true;
    }
  }

  apresentarMensageDeValidacao(titulo, mensagem) {
    this.alerta = new Alerta();
    this.alerta.titulo = titulo;
    this.alerta.mensagem = mensagem;
    this.alertaService.apresentarAlertaDeSucesso(this.alerta);
  }

  async apresentarObrigado() {
    this.navCrtl.pop();
    this.modal = await this.modalController.create({
      component: ObrigadoPage,
      componentProps: { 'current': 'denuncia', 'EhProcon': false }
    });
    this.modalService.setModal(this.modal);
    return await this.modal.present();
  }

}
