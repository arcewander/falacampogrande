import { TestBed } from '@angular/core/testing';

import { MenuSuperiorService } from './menu-superior.service';

describe('MenuSuperiorService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MenuSuperiorService = TestBed.get(MenuSuperiorService);
    expect(service).toBeTruthy();
  });
});
