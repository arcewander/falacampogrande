import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuSuperiorPage } from './menu-superior.page';

describe('MenuSuperiorPage', () => {
  let component: MenuSuperiorPage;
  let fixture: ComponentFixture<MenuSuperiorPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenuSuperiorPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuSuperiorPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
