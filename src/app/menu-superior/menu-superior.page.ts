import { Component, OnInit } from '@angular/core';
import { Menu } from '../model/Menu';
import { Usuario } from '../model/Usuario';
import { Notificacao } from '../model/Notificacao';
import { NavController } from '@ionic/angular';
import { Router } from '@angular/router';
import { NotificacaoService } from '../notificacao/notificacao.service';
import { LoginService } from '../login/login.service';
import { MenuSuperiorService } from './menu-superior.service';

@Component({
  selector: 'app-menu-superior',
  templateUrl: './menu-superior.page.html',
  styleUrls: ['./menu-superior.page.scss'],
})
export class MenuSuperiorPage implements OnInit {

  menu: Menu = new Menu();
  quantidade: any = 0;
  usuario: Usuario;
  backButton: boolean;
  notificacoes: Array<Notificacao> = new Array<Notificacao>();

  constructor(private navCtrl: NavController,
              private router: Router,
              private notificacaoService: NotificacaoService,
              private loginService: LoginService,
              private menuSuperiorService: MenuSuperiorService) {

  }

  ngOnInit(): void {
    this.menu = this.menuSuperiorService.recuperaInformacoesMenu();

  }

  ngOnAfterView() {
    this.usuario = this.loginService.getUsuario();
    if  (this.usuario !== undefined && this.usuario !== null) {
      this.atualizarNotificacao();
    }
  }

  async atualizarNotificacao() {
    console.log(1);
    await this.notificacaoService.buscar(this.usuario.coduso).subscribe((notificacoes: Array<Notificacao>) => {
      this.notificacoes = notificacoes;
      this.atualizar(notificacoes);
      setTimeout(() => {
        this.menuSuperiorService.atualizaQuantidade((quant) => {
          console.info(quant);
          if (quant === undefined || quant === null){
            this.quantidade = 0;
          }
          this.menu.quantidade = quant;
        });
      }, 3000);
    });
  }

  voltarParaPaginaInicial() {
    this.navCtrl.navigateRoot('tela-inicial');
  }

  irParaNotificacao() {
    this.router.navigate(['notificacao']);
  }

  contarNotificacoesNaoLidas() {
    this.quantidade = this.notificacaoService.contarNotificacoesNaoLidas(() => { });
  }

  notificacoesNaoLidas() {
    this.notificacaoService.notificacoesNaoLidas(data => console.log('Não lidas: ', data));
  }

  atualizar(notificacoes) {
    if (notificacoes == null || notificacoes == undefined) return;
    for (let index = 0; index < notificacoes.length; index++) {
      this.notificacaoService.sincronizar(notificacoes[index]);
    }
  }

}
