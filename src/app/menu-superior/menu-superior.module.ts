import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { MenuSuperiorPage } from './menu-superior.page';

const routes: Routes = [
  {
    path: '',
    component: MenuSuperiorPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
  ],
  declarations: [MenuSuperiorPage],
  exports: [MenuSuperiorPage]
})
export class MenuSuperiorPageModule {}
