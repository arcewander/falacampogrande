import { Injectable } from '@angular/core';
import { Menu } from '../model/Menu';
import { NotificacaoService } from '../notificacao/notificacao.service';

@Injectable({
  providedIn: 'root'
})
export class MenuSuperiorService {

  menu: Menu = new Menu();
  funAtualizaQuantidade: any;

  constructor(private notificacaoService: NotificacaoService) { }

  setTitle(titulo: string, subTitulo?: string) {
    this.menu.titulo = titulo;
    this.menu.subTitulo = subTitulo;
    this.atualizaQuantidade();
    console.log(this.menu);
  }

  async atualizaQuantidade(callback?) {
    console.log(2);
    alert(2);
    return this.notificacaoService.contarNotificacoesNaoLidas(cal => {
      if (!this.funAtualizaQuantidade) {
        this.funAtualizaQuantidade = callback;
        this.menu.quantidade = callback;
      }
      this.funAtualizaQuantidade = (cal);
      this.menu.quantidade = this.funAtualizaQuantidade;
    });
  }

  recuperaInformacoesMenu() {
    return this.menu;
  }

  showBackButton(show: boolean) {
    this.menu.backButton = show;
  }

}
