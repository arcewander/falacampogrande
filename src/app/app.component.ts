import { Component } from '@angular/core';

import { Platform, NavController, PickerController, ModalController, ToastController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { SQLiteObject } from '@ionic-native/sqlite/ngx';
import { LoginService } from './login/login.service';
import { Router } from '@angular/router';
import { ModalService } from './modal-service/modal.service';
import { DatabaseService } from './database/database.service';
import { AguardeService } from './aguarde/aguarde.service';
import { AmbienteService } from './ambiente/ambiente.service';
import { Usuario } from './model/Usuario';
import { Ambiente } from './model/Ambiente';


@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {

  usuario: Usuario;
  component: Component;
  ambiente: Ambiente;
  constructor(
    private platform: Platform,
    //private pushService: PushService,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private loginService: LoginService,
    private navCtrl: NavController,
    private router: Router,
    private pickerController: PickerController,
    public modalController: ModalController,
    private modalService: ModalService,
    public toastController: ToastController,
    public databaseService: DatabaseService,
    private aguardeService: AguardeService,
    private ambienteService: AmbienteService
  ) {

    this.initializeApp();
  }

  initializeApp() {
    this.splashScreen.show();
    this.platform.ready().then(() => {
      this.statusBar.styleLightContent();

      if (this.platform.is('ios') || this.platform.is('android')) {
        this.databaseService.abrirConexao().then((db: SQLiteObject) => {
          this.databaseService.setDatabase(db);
          this.databaseService.criarTabelas().then(() => {
            this.ambienteService.ambienteAtual((ambiente) => {
              this.ambiente = ambiente;
            });
          });

        }).catch(error => {
          console.error(error);
        });
        setTimeout(() => this.buscarUsuario(), 2500);
      }
      this.splashScreen.hide();
      this.platform.backButton.subscribe(() => {
        if (this.router.isActive('tela-inicial', true) ||
          this.router.isActive('login', true) ||
          this.aguardeService.loading) {
          //return;
        } else if (this.modalService.existeModalAberto()) {
          this.modalController.dismiss();
          this.modalService.remover();
        }
        else {
          let picker = this.pickerController.getTop();
          if (picker != null || picker !== undefined) {
            this.pickerController.dismiss();
          } else {
            this.navCtrl.back();
          }
          return;
        }
      });
      this.notificationSetup();
      this.splashScreen.hide();
    });
  }

  buscarUsuario() {
    this.loginService.buscarUsuario((usuario) => {
      if (usuario != null || usuario !== undefined) {
        this.loginService.login(usuario).subscribe( usuario => {
          if (usuario != null || usuario !== undefined) {
            this.irTelaPrincipal();
          }
        },
          error => { console.log(error); });
      }
    });
  }

  irTelaPrincipal() {
    this.navCtrl.navigateRoot('/tela-inicial');
  }

  private async presentToast(message) {
    const toast = await this.toastController.create({
      message,
      duration: 3000
    });
    toast.present();
  }

  private notificationSetup() {
    //this.pushService.getToken();
    /*this.pushService.onNotifications().subscribe(
      (msg) => {
        if (this.platform.is('ios')) {
          this.presentToast(msg.aps.alert);
        } else {
          this.presentToast(msg.body);
        }
      });*/
  }
}
