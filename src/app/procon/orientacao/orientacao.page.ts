import { Component, OnInit } from '@angular/core';
import { RamoAtividadeService } from './../../ramo-atividade/ramo-atividade.service';
import { SolicitacaoProcon } from './../../model/SolicitacaoProcon';
import { RamoAtividade } from '../../model/RamoAtividade';
import { ModalController, Platform, NavController } from '@ionic/angular';
import { RamoAtividadeModalPage } from '../../ramo-atividade-modal/ramo-atividade-modal.page';
import { EnderecoModalPage } from '../endereco-modal/endereco-modal.page';
import { ObrigadoPage } from '../../obrigado/obrigado.page';
import { ModalService } from '../../modal-service/modal.service';
import { OrientacaoService } from './orientacao.service';
import { AguardeService } from '../../aguarde/aguarde.service';
import { Endereco } from '../../model/Endereco';
import { Arquivo } from '../../model/Arquivo';
import { ArquivoProcon } from '../../model/ArquivoProcon';
import { AlertaService } from '../../alerta/alerta.service';
import { Alerta } from '../../model/Alerta';

import { Usuario } from '../../model/Usuario';
import { LoginService } from '../../login/login.service';
import { TelaInicialService } from '../../tela-inicial/tela-inicial.service';
import { CameraService } from 'src/app/camera-service/camera-service.service';
import { ArquivoService } from 'src/app/arquivo-service/arquivo-service.service';

@Component({
  selector: 'app-orientacao',
  templateUrl: './orientacao.page.html',
  styleUrls: ['./orientacao.page.scss'],
})
export class OrientacaoPage implements OnInit {

  solicitacao: SolicitacaoProcon;
  ramoAtividades: Array<RamoAtividade>;
  modal: HTMLIonModalElement;
  arquivo: Arquivo;
  arquivos: Array<Arquivo> = new Array<Arquivo>();
  ramoSelecionado: RamoAtividade;
  anpVO: ArquivoProcon = new ArquivoProcon();
  anpVOs: Array<ArquivoProcon> = new Array<ArquivoProcon>();
  alerta: Alerta = new Alerta();
  usuario: Usuario = new Usuario();

  constructor(private ramoAtividadeService: RamoAtividadeService,
              public modalController: ModalController,
              private platform: Platform,
              private navCtrl: NavController,
              private aguardeService: AguardeService,
              private modalService: ModalService,
              private cameraService: CameraService,
              private arquivoService: ArquivoService,
              private alertaService: AlertaService,
              private orientacaoService: OrientacaoService,
              private telaInicialService: TelaInicialService,
              private loginService: LoginService) {
    this.solicitacao = new SolicitacaoProcon();
    this.usuarioLogado();
    this.solicitacao.tpeden = 1;
    this.solicitacao.tipden = 1;
    this.ramoSelecionado = new RamoAtividade();


    this.ramoAtividadeService.buscar().then((ramoAtividades: Array<RamoAtividade>) => {

      this.ramoAtividades = ramoAtividades.sort((a, b) => {
        return a.nomram.localeCompare(b.nomram.toString());
      });

    });
  }

  ngOnInit() {
  }

  async apresentarModal() {
    this.modal = await this.modalController.create({
      component: RamoAtividadeModalPage,
      componentProps: { 'lista': this.ramoAtividades }
    });
    this.modalService.setModal(this.modal);
    return await this.modal.present();
  }

  selecionarRamoAtividade() {

    this.apresentarModal().then(() => {

      this.modal.onDidDismiss().then(item => {

        if (item.data != null) {
          this.platform.backButton.subscribe(() => {
            this.navCtrl.back();
          })

          this.ramoSelecionado = <RamoAtividade>item.data;
          this.ramoSelecionado.nomram = this.ramoSelecionado.nomram.toUpperCase();
          this.solicitacao.codram = this.ramoSelecionado.codram;

        } else {
          console.log('NULL');
        }
      }).catch((erro) => { console.log(erro); });

    }).catch((erro) => { console.log(erro); });

  }

  capturarFoto() {
    if (this.solicitacao.anpVOs.length < 7) {
      this.cameraService.capturarFoto().then((arquivo: Arquivo) => {
        this.arquivo = arquivo;
        this.vincularArquivos(this.arquivo);
      },
        error => { console.log(error) });
    } else {
      this.alerta.titulo = 'Atenção';
      this.alerta.mensagem = 'Pemitido máximo 7 anexos';
    }
  }

  async anexarArquivo() {
    if (this.solicitacao.anpVOs.length < 7) {
      this.cameraService.selecionarImagem().then((arquivo: Arquivo) => {
        this.arquivo = arquivo;
        this.arquivos.push(this.arquivo);

        this.vincularArquivos(this.arquivo);

      }
        , error => { console.log(error) });
    } else {
      this.alerta.titulo = 'Atenção';
      this.alerta.mensagem = 'Maximo 7 anexos';
    }
  }

  async vincularArquivos(arquivo) {
    this.aguardeService.processar('Anexando o Arquivo').then(() => {
      this.anpVO = new ArquivoProcon();
      this.anpVO.nomanp = arquivo.nome;
      this.anpVO.tipanp = 0;
      this.anpVO.usualt = this.solicitacao.coduso.toString();
      this.anpVO.arqbase64 = arquivo.arquivoUrl;
      this.anpVO.formato = arquivo.tipo;
      this.solicitacao.anpVOs.push(this.anpVO);
      console.log(this.solicitacao.anpVOs);
      this.aguardeService.encerrar();
    });
  }

  async removerAnexo(arquivo) {

    this.solicitacao.anpVOs.forEach((item, index) => {
      console.log(item === arquivo);
      if (item === arquivo) this.solicitacao.anpVOs.splice(index, 1);
    });
    console.log('removeu? ');
    console.log(this.solicitacao.anpVOs);
  }

  validarSolicitacao() {
    if (this.solicitacao.desden == undefined || this.solicitacao.desden == null) {
      this.apresentarAlerta('Atenção', 'Campo Ocorrência obrigatório');
      return false;
    }

    if (this.solicitacao.codram == 0) {
      this.apresentarAlerta('Atenção', 'Campo Ramo de Atividade obrigatório');
      return false;
    }

    if (this.solicitacao.anpVOs.length == 0) {
      this.apresentarAlerta('Atenção', 'Por favor anexar foto(s) ou arquivo(s)!');
      return false;
    } else {
      return true;
    }

  }

  apresentarAlerta(titulo, mensagem) {
    this.alerta = new Alerta();
    this.alerta.titulo = titulo;
    this.alerta.mensagem = mensagem;
    this.alertaService.apresentarAlertaDeAviso(this.alerta, () => { })
  }

  async apresentarObrigado() {
    this.modal = await this.modalController.create({
      component: ObrigadoPage,
      componentProps: { 'current': 'orientacao',
    'EhProcon' : true }
    });
    this.modalService.setModal(this.modal);
    return await this.modal.present();
  }

  salvar() {
    let valido = this.validarSolicitacao();
    if (valido == true) {
      this.aguardeService.processar('Enviando Solicitação').then(() => {
        this.orientacaoService.enviar(this.solicitacao).subscribe((obj: any) => {

          this.aguardeService.encerrar();
          this.alerta = new Alerta();
          this.alerta.titulo = 'Protocolo: ' + obj.protocolo;
          this.alerta.mensagem = 'Campo Grande agradece, esta é a cidade que todos queremos!';
          this.alertaService.apresentarAlertaDeSucesso(this.alerta, () => {
            this.apresentarObrigado();
            this.limpar();
          });
        },
          error => {
            this.alerta = new Alerta();
            this.alerta = error;
            this.alertaService.apresentarAlertaDeErro(this.alerta, () => { });

            this.aguardeService.encerrar();
          });

      },
        error => {
          this.alerta = new Alerta();
          this.alerta = error;
          this.alertaService.apresentarAlertaDeErro(this.alerta, () => { });
          this.aguardeService.encerrar();
        });
    }
  }

  limpar() {
    this.solicitacao = new SolicitacaoProcon();
    this.usuarioLogado();
    this.solicitacao.desden = '';
    this.ramoSelecionado = new RamoAtividade();
    this.solicitacao.tpeden = 1;
    this.solicitacao.tipden = 1;
  }

  usuarioLogado() {
    this.usuario = this.loginService.getUsuario();
    this.solicitacao.coduso = this.usuario.coduso;
  }
}
