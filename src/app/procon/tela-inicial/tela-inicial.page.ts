import { Component, OnInit } from '@angular/core';

import { ModalController, Platform, NavController } from '@ionic/angular';
import { AbrirLinkService } from '../../abrir-link/abrir-link.service';

import { ContatoService } from '../contato/contato.service';
import { ModalService } from '../../modal-service/modal.service';

import { Contato } from '../../model/Contato';
import { TermoPage } from '../../termo/termo.page';
import { TelaInicialService } from '../../tela-inicial/tela-inicial.service';
import { MenuSuperiorService } from '../../menu-superior/menu-superior.service';

@Component({
  selector: 'app-tela-inicial',
  templateUrl: './tela-inicial.page.html',
  styleUrls: ['./tela-inicial.page.scss'],
})
export class TelaInicialPage implements OnInit {

  link: String = "https://www.facebook.com/proconcg/";
  contatos: Array<Contato> = new Array<Contato>();
  modal: HTMLIonModalElement;
  constructor(private navCtrl: NavController,
    private abrirLinkService: AbrirLinkService,
    private contatoService: ContatoService,
    public modalController: ModalController,
    private telaInicialService: TelaInicialService,
    private menuSuperiorService: MenuSuperiorService,
    private modalService: ModalService) { }

  ngOnInit() {

  }

  ionViewWillEnter(){
    this.menuSuperiorService.setTitle("Procon", "");
    this.menuSuperiorService.showBackButton(true);    
  }
  
  ngOnDestroy() {
    this.telaInicialService.scrollIntoView();
  }

  abrirLink() {
    this.abrirLinkService.abrir(this.link);
  }

  async irParaContato() {
    this.navCtrl.navigateForward("contato");
  }

  async irParaOrientacao() {
    this.modal = await this.modalController.create({
      component: TermoPage,
      componentProps: { "codtpt": 2 }
    });
    this.modalService.setModal(this.modal);
    return await this.modal.present();
  }


  async irParaDenuncia() {
    this.modal = await this.modalController.create({
      component: TermoPage,
      componentProps: { "codtpt": 5 }
    });
    this.modalService.setModal(this.modal);
    return await this.modal.present();
  }

  async irParaInformacoesAoConsumidor() {
    this.modal = await this.modalController.create({
      component: TermoPage,
      componentProps: { "codtpt": 1 }
    });
    this.modalService.setModal(this.modal);
    return await this.modal.present();
  }


}