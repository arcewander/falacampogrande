import { TestBed } from '@angular/core/testing';

import { InformacoesConsumidorService } from './informacoes-consumidor.service';

describe('InformacoesConsumidorService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: InformacoesConsumidorService = TestBed.get(InformacoesConsumidorService);
    expect(service).toBeTruthy();
  });
});
