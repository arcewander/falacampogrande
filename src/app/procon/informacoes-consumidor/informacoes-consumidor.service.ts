import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';

import { InformacaoConsumidor } from '../../model/InformacaoConsumidor';

import { AmbienteService } from '../../ambiente/ambiente.service';
import { Ambiente } from '../../model/Ambiente';

@Injectable({
	providedIn: 'root'
})
export class InformacoesConsumidorService {

	private informacaoConsumidor: InformacaoConsumidor = new InformacaoConsumidor();
	ambiente: Ambiente = new Ambiente();

	constructor(private http: HttpClient, private ambienteService: AmbienteService) {
		this.ambiente = this.ambienteService.getAmbiente();
	}

	cadastrar(informacaoConsumidor: InformacaoConsumidor) {
		let informacao = new InformacaoConsumidor();
		informacao.cadrei = 1;
		informacao.coduso = informacaoConsumidor.coduso;
		informacao.atvrei = informacaoConsumidor.atvrei;

		return this.http.post(this.ambiente.url + "receberinformacao/web/incluir",
			informacao,
			{ headers: { 'Content-Type': 'application/json' } })
			.pipe(
				map(informacaoConsumidor => {
					this.informacaoConsumidor = <InformacaoConsumidor>informacaoConsumidor;

					this.verificarSeOUsuarioJaEstaInscrito(this.informacaoConsumidor.coduso);
				})
			);
	}

	verificarSeOUsuarioJaEstaInscrito(coduso) {
		return this.http.get(this.ambiente.url + "receberinformacao/coduso/" + coduso).pipe(
			map(informacaoConsumidor => {
				console.log("response endpoint : " + JSON.stringify(informacaoConsumidor));
				if (informacaoConsumidor == null) {
					console.log("return null");
					return null;
				}
				console.log("return obj ");
				return this.informacaoConsumidor = <InformacaoConsumidor>informacaoConsumidor
			}));
	}

	cancelarCadastro(coduso) {
		return this.http.get(this.ambiente.url + "receberinformacao/cancelar/" + coduso).pipe(
			map(informacaoConsumidor =>
				this.informacaoConsumidor = <InformacaoConsumidor>informacaoConsumidor
			));
	}


}
