import { Component, OnInit } from '@angular/core';

import { InformacaoConsumidor } from '../../model/InformacaoConsumidor';
import { Usuario } from '../../model/Usuario';
import { InformacoesConsumidorService } from './informacoes-consumidor.service';
import { LoginService } from '../../login/login.service';
import { MenuSuperiorService } from '../../menu-superior/menu-superior.service';

@Component({
  selector: 'app-informacoes-consumidor',
  templateUrl: './informacoes-consumidor.page.html',
  styleUrls: ['./informacoes-consumidor.page.scss'],
})
export class InformacoesConsumidorPage implements OnInit {

  informacaoConsumidor: InformacaoConsumidor = new InformacaoConsumidor();
  usuario: Usuario;
  cadastroExistente: boolean;


  constructor(private informacoesConsumidorService: InformacoesConsumidorService,
              private menuSuperiorService: MenuSuperiorService,
              private loginService: LoginService) {
    this.getUsuario();
    this.verificarCadastro();

  }
  ionViewWillEnter() {
    this.menuSuperiorService.setTitle('Informações ao Consumidor', '');
    this.menuSuperiorService.showBackButton(true);
  }
  ngOnInit() {
  }

  getUsuario() {
    this.usuario = this.loginService.getUsuario();
  }


  cadastrar() {
    this.informacaoConsumidor.atvrei = !this.informacaoConsumidor.atvrei;
    this.informacaoConsumidor.coduso = this.usuario.coduso;
    this.informacoesConsumidorService.cadastrar(this.informacaoConsumidor)
      .subscribe(informacaoConsumidor => {

      });
  }



  verificarCadastro() {
    console.log('usuario  :' + this.usuario.coduso);
    this.informacoesConsumidorService.verificarSeOUsuarioJaEstaInscrito(this.usuario.coduso)
      .subscribe(informacaoConsumidor => {
        console.log('resposta obj :' + informacaoConsumidor);
        if (informacaoConsumidor == null || informacaoConsumidor === undefined) {
          const inf = new InformacaoConsumidor();
          inf.atvrei = false;
          this.informacaoConsumidor = inf;
        } else {
          console.log('resposta com inf:' + JSON.stringify(informacaoConsumidor));
          this.informacaoConsumidor = <InformacaoConsumidor> informacaoConsumidor;
        }
      });
  }

  cancelar() {
    this.informacoesConsumidorService.cancelarCadastro(this.usuario.coduso)
      .subscribe(informacaoConsumidor => {
        this.informacaoConsumidor = <InformacaoConsumidor> informacaoConsumidor;
      });
  }
}
