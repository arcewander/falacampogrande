import { Injectable } from '@angular/core';

import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';

import { SolicitacaoProcon } from '../../model/SolicitacaoProcon';

import { AmbienteService } from '../../ambiente/ambiente.service';
import { Ambiente } from '../../model/Ambiente';
import { Usuario } from './../../model/Usuario';
import { LoginService } from './../../login/login.service';

@Injectable({
	providedIn: 'root'
})
export class MinhasSolicitacoesProconService {

	solicitacoesProcon: Array<SolicitacaoProcon> = new Array<SolicitacaoProcon>();
	ambiente: Ambiente = new Ambiente();
	usuario: Usuario = new Usuario();

  constructor(private http: HttpClient,
              private ambienteService: AmbienteService,
	             private loginService: LoginService) {
		  this.usuario = this.loginService.getUsuario();
	}

  buscar(coduso) {
	  this.ambiente = this.ambienteService.getAmbiente();
	  return this.http.get(this.ambiente.url + 'denuncia/den/coduso/' + coduso)
		  .pipe(map(solicitacoesProcon => this.solicitacoesProcon = solicitacoesProcon as Array<SolicitacaoProcon>));
	}


}
