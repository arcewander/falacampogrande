import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MinhasSolicitacoesPage } from './minhas-solicitacoes.page';

describe('MinhasSolicitacoesPage', () => {
  let component: MinhasSolicitacoesPage;
  let fixture: ComponentFixture<MinhasSolicitacoesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MinhasSolicitacoesPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MinhasSolicitacoesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
