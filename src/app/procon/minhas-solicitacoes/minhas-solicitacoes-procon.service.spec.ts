import { TestBed } from '@angular/core/testing';

import { MinhasSolicitacoesProconService } from './minhas-solicitacoes-procon.service';

describe('MinhasSolicitacoesProconService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MinhasSolicitacoesProconService = TestBed.get(MinhasSolicitacoesProconService);
    expect(service).toBeTruthy();
  });
});
