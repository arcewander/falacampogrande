import { Component, OnInit } from '@angular/core';
import { MinhasSolicitacoesProconService } from './minhas-solicitacoes-procon.service';
import { SolicitacaoProcon } from '../../model/SolicitacaoProcon';
import { NavController } from '@ionic/angular';
import { Usuario } from '../../model/Usuario';
import { LoginService } from '../../login/login.service';

@Component({
  selector: 'app-minhas-solicitacoes',
  templateUrl: './minhas-solicitacoes.page.html',
  styleUrls: ['./minhas-solicitacoes.page.scss'],
})
export class MinhasSolicitacoesPage implements OnInit {

  usuario: Usuario;

  solicitacoesProcon: Array<SolicitacaoProcon>;
  public detalhes: boolean = false;

  constructor(private minhasSolicitacoesProconService: MinhasSolicitacoesProconService,
              private usuarioService: LoginService,
              private navCtrl: NavController) {
    this.usuario = usuarioService.getUsuario();
    this.buscar();
  }

  ngOnInit() {

  }

  buscar() {
    this.minhasSolicitacoesProconService.buscar(this.usuario.coduso)
      .subscribe((solicitacoesProcon: Array<SolicitacaoProcon>) => {
        this.solicitacoesProcon = solicitacoesProcon;
        console.info(this.solicitacoesProcon);
      }
        , error => { console.log(error); });
  }

  mostrarDetalhes(codsol) {
    this.detalhes = codsol;
  }

  esconderDetalhes() {
    this.detalhes = false;
  }

  irParaLegenda() {
    this.navCtrl.navigateForward('legenda');
  }
}
