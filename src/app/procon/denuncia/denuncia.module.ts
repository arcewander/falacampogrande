import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { DenunciaPage } from './denuncia.page';
import { MenuSuperiorPageModule } from '../../menu-superior/menu-superior.module';

const routes: Routes = [
  {
    path: '',
    component: DenunciaPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    MenuSuperiorPageModule
  ],
  declarations: [DenunciaPage]
})
export class DenunciaPageModule {}
