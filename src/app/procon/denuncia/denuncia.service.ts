import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SolicitacaoProcon } from '../../model/SolicitacaoProcon'

import { AmbienteService } from '../../ambiente/ambiente.service';
import { Ambiente } from '../../model/Ambiente';

@Injectable({
  providedIn: 'root'
})
export class DenunciaService {

  ambiente: Ambiente = new Ambiente();

  constructor(private http: HttpClient, private ambienteService: AmbienteService) {

  }

  enviar(solicitacao: SolicitacaoProcon) {
    this.ambiente = this.ambienteService.getAmbiente();

    return this.http.post(this.ambiente.url + 'denuncia/mobile/incluir',
      solicitacao,
      { headers: { 'Content-Type': 'application/json' } }
    );
  }



}
