import { Component, OnInit } from '@angular/core';
import { SolicitacaoProcon } from './../../model/SolicitacaoProcon';
import { RamoAtividade } from '../../model/RamoAtividade';
import { ModalController, Platform, NavController, IonProgressBar } from '@ionic/angular';
import { RamoAtividadeModalPage } from '../../ramo-atividade-modal/ramo-atividade-modal.page';
import { EnderecoModalPage } from '../endereco-modal/endereco-modal.page';
import { ObrigadoPage } from '../../obrigado/obrigado.page';
import { EnderecoService } from '../endereco/endereco.service';
import { ModalService } from '../../modal-service/modal.service';
import { DenunciaService } from './denuncia.service';
import { AguardeService } from '../../aguarde/aguarde.service';
import { RamoAtividadeService } from './../../ramo-atividade/ramo-atividade.service';
import { Endereco } from '../../model/Endereco';
import { Arquivo } from '../../model/Arquivo';
import { ArquivoProcon } from '../../model/ArquivoProcon';
import { AlertaService } from '../../alerta/alerta.service';
import { Alerta } from '../../model/Alerta';
import { Usuario } from '../../model/Usuario';
import { LoginService } from '../../login/login.service';
import { Router } from '@angular/router';
import { MenuSuperiorService } from '../../menu-superior/menu-superior.service';
import { CameraService } from '../../camera-service/camera-service.service';
import { ArquivoService } from '../../arquivo-service/arquivo-service.service';

@Component({
  selector: 'app-denuncia',
  templateUrl: './denuncia.page.html',
  styleUrls: ['./denuncia.page.scss'],
})
export class DenunciaPage {

  solicitacao: SolicitacaoProcon;
  ramoAtividades: Array<RamoAtividade>;
  enderecos: Array<Endereco>;
  modal: HTMLIonModalElement;
  arquivo: Arquivo;
  arquivos: Array<Arquivo> = new Array<Arquivo>();
  ramoSelecionado: RamoAtividade;
  logradouro: String;
  anpVO: ArquivoProcon = new ArquivoProcon();
  anpVOs: Array<ArquivoProcon> = new Array<ArquivoProcon>();
  alerta: Alerta = new Alerta();
  usuario: Usuario = new Usuario();

  constructor(
    private enderecoService: EnderecoService,
    private ramoAtividadeService: RamoAtividadeService,
    public modalController: ModalController,
    private platform: Platform,
    private navCtrl: NavController,
    private aguardeService: AguardeService,
    private modalService: ModalService,
    private cameraService: CameraService,
    private arquivoService: ArquivoService,
    private menuSuperiorService: MenuSuperiorService,
    private denunciaService: DenunciaService,
    private alertaService: AlertaService,
    private loginService: LoginService
  ) {

    this.solicitacao = new SolicitacaoProcon();

    this.usuarioLogado();
    this.solicitacao.tpeden = 1;
    this.solicitacao.tipden = 0;

    this.ramoSelecionado = new RamoAtividade();


    this.ramoAtividadeService.buscar().then((ramoAtividades: Array<RamoAtividade>) => {

      this.ramoAtividades = ramoAtividades.sort((a, b) => {
        return a.nomram.localeCompare(b.nomram.toString());
      });

    });
  }

  async apresentarModal() {
    this.modal = await this.modalController.create({
      component: RamoAtividadeModalPage,
      componentProps: { 'lista': this.ramoAtividades }
    });
    this.modalService.setModal(this.modal);
    return await this.modal.present();
  }

  selecionarRamoAtividade() {

    this.apresentarModal().then(() => {

      this.modal.onDidDismiss().then(item => {

        if (item.data != null) {

          this.ramoSelecionado = <RamoAtividade>item.data;
          this.ramoSelecionado.nomram = this.ramoSelecionado.nomram.toUpperCase();
          this.solicitacao.codram = this.ramoSelecionado.codram;

        } else {
        }
      }).catch((erro) => { console.log(erro); });

    }).catch((erro) => { console.log(erro); });

  }

  async apresentarModalEndereco() {
    this.modal = await this.modalController.create({
      component: EnderecoModalPage,
      componentProps: { 'lista': this.enderecos }
    });
    this.modalService.setModal(this.modal);
    return await this.modal.present();
  }

  selecionarEndereco(endereco: any) {
    var list = document.getElementById('enderecos');

    list.hidden = true;

    this.logradouro = endereco.cepend + ' - ' + endereco.logend + ', ' + endereco.baiend;

    this.solicitacao.epcVO.cepepc = endereco.cepend;
    this.solicitacao.epcVO.lgdepc = endereco.logend;
    this.solicitacao.epcVO.bairroepc = endereco.baiend;
  }

  async buscarEndereco() {
    var list = document.getElementById('enderecos');
    var gif = document.getElementById('loadGif');

    gif.hidden = false;
    this.enderecos = new Array<Endereco>();
    let valorSelecionado = this.solicitacao.epcVO.cepepc + ' - ' + this.solicitacao.epcVO.lgdepc + ', ' + this.solicitacao.epcVO.bairroepc;

    if (this.logradouro == '' || this.logradouro == undefined || valorSelecionado == this.logradouro) {
      list.hidden = true;
      return;
    }

    if (this.logradouro.length > 3) {
      this.enderecoService.buscarPor(this.logradouro).subscribe((enderecos: Array<Endereco>) => {
        this.enderecos = enderecos.slice(0, 5);
        gif.hidden = true;
      }
        , error => {
          gif.hidden = true;
          console.log(error);
        });
    }

    list.hidden = false;

    return;
    // this.apresentarModalEndereco().then(() => {

    //   this.modal.onDidDismiss().then(item => {

    //     if (item.data != null) {
    //       this.logradouro = item.data.cepend + ', ' + item.data.logend + ', ' + item.data.baiend;
    //       this.solicitacao.epcVO.cepepc = item.data.cepend;
    //       this.solicitacao.epcVO.lgdepc = item.data.logend;
    //       this.solicitacao.epcVO.bairroepc = item.data.baiend;
    //     }
    //   }).catch((erro) => { console.log(erro); });

    // }).catch((erro) => { console.log(erro); });

  }
  ionViewWillEnter() {
    this.menuSuperiorService.setTitle('Denúncia', '');
    this.menuSuperiorService.showBackButton(true);
  }

  async salvar() {
    let valido = this.validarSolicitacao();
    if (valido == true) {
      await this.aguardeService.processar('Enviando Solicitação');
      this.denunciaService.enviar(this.solicitacao).subscribe((alerta: Alerta) => {
        this.alerta = alerta;
        this.aguardeService.encerrar();
        this.alertaService.apresentarAlertaDeSucesso(this.alerta, () => {
          this.apresentarObrigado();
          this.limpar();
        });
      });
    };
    error => {
      console.log(error);
      this.aguardeService.encerrar();
    }
  }

  capturarFoto() {
    if (this.solicitacao.anpVOs.length < 7) {

      //this.aguardeService.processar('Anexando o Arquivo');
      this.platform.resume.subscribe(() => this.aguardeService.encerrar());
      this.cameraService.capturarFoto().then((arquivo: Arquivo) => {
        console.log(JSON.stringify(arquivo));
        this.arquivo = arquivo;
        this.vincularArquivos(this.arquivo);
        //this.aguardeService.encerrar();
        this.platform.resume.subscribe(() => { });
      },
        error => {
          console.log(error);
          //this.aguardeService.encerrar();
          this.platform.resume.subscribe(() => { });
        });
    } else {
      this.alerta.titulo = 'Atenção';
      this.alerta.mensagem = 'Pemitido máximo 7 anexos';
    }
  }

  async anexarArquivo() {
    if (this.solicitacao.anpVOs.length < 7) {
      this.cameraService.selecionarImagem().then((arquivo: Arquivo) => {
        this.arquivo = arquivo;
        this.arquivos.push(this.arquivo);
        this.vincularArquivos(this.arquivo);
        this.aguardeService.encerrar();
      }
        , error => {
          this.apresentarAlerta('Atenção', error);
          this.aguardeService.encerrar();
        });
    } else {
      this.apresentarAlerta('Atenção', 'Maximo 7 anexos');
    }
  }

  async vincularArquivos(arquivo) {
    this.anpVO = new ArquivoProcon();
    this.anpVO.nomanp = arquivo.nome;
    this.anpVO.tipanp = 0;
    this.anpVO.usualt = this.solicitacao.coduso.toString();
    this.anpVO.arqbase64 = arquivo.arquivoUrl;
    this.anpVO.formato = arquivo.formato;
    this.solicitacao.anpVOs.push(this.anpVO);
  }

  async removerAnexo(arquivo) {
    this.solicitacao.anpVOs.forEach((item, index) => {
      if (item === arquivo) this.solicitacao.anpVOs.splice(index, 1);
    });

  }

  async apresentarObrigado() {
    this.modal = await this.modalController.create({
      component: ObrigadoPage,
      componentProps: { 'current': 'denuncia', 'EhProcon': true }
    });
    this.modalService.setModal(this.modal);
    return await this.modal.present();
  }

  validarSolicitacao() {
    if (this.solicitacao.desden == undefined || this.solicitacao.desden == null) {
      this.apresentarAlerta('Atenção', 'Campo Ocorrência obrigatório');
      return false;
    }
    if (this.solicitacao.estden == undefined || this.solicitacao.estden == null) {
      this.apresentarAlerta('Atenção', 'Campo Local da Ocorrência obrigatório');
      return false;
    }
    if (this.solicitacao.epcVO.lgdepc == undefined || this.solicitacao.epcVO.lgdepc == null) {
      this.apresentarAlerta('Atenção', 'Campo Endereço da Ocorrência obrigatório');
      return false;
    }
    if (this.solicitacao.codram == 0) {
      this.apresentarAlerta('Atenção', 'Campo Ramo de Atividade obrigatório');
      return false;
    }
    if (this.solicitacao.anpVOs.length <= 0) {
      this.apresentarAlerta('Atenção', 'Você deve adicionar no mínimo uma foto');
      return false;
    }
    if (this.solicitacao.epcVO.numeroepc == null) {
      this.apresentarAlerta('Atenção', 'Campo Número obrigatório');
      return false;
    } else {
      return true;
    }

  }

  apresentarAlerta(titulo, mensagem) {
    this.alerta = new Alerta();
    this.alerta.titulo = titulo;
    this.alerta.mensagem = mensagem;
    this.alertaService.apresentarAlertaDeAviso(this.alerta, () => { })
  }

  usuarioLogado() {
    this.usuario = this.loginService.getUsuario();
    this.solicitacao.coduso = this.usuario.coduso;
  }
  limpar() {
    this.solicitacao = new SolicitacaoProcon();
    this.usuarioLogado();
    this.solicitacao.desden = '';
    this.solicitacao.estden = '';
    this.ramoSelecionado = new RamoAtividade();
    this.solicitacao.tpeden = 1;
    this.solicitacao.tipden = 0;
    this.logradouro = '';
  }
}
