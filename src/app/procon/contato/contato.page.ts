import { Component, OnInit } from '@angular/core';

import { EnderecoService } from '../endereco/endereco.service';
import { ContatoService } from '../contato/contato.service';
import { TipoContatoService } from '../tipo-contato/tipo-contato.service';
import { AbrirLinkService } from '../../abrir-link/abrir-link.service';
import { LigarService } from '../../ligar/ligar.service';

import { Contato } from '../../model/Contato';
import { TipoContato } from '../../model/TipoContato';
import { Endereco } from '../../model/Endereco';
import { MenuSuperiorService } from '../../menu-superior/menu-superior.service';
import { OuvidoriaService } from '../../ouvidoria/ouvidoria.service';
import { Ouvidoria } from '../../model/Ouvidoria';


@Component({
	selector: 'app-contato',
	templateUrl: './contato.page.html',
	styleUrls: ['./contato.page.scss'],
})
export class ContatoPage implements OnInit {

	enderecosProcon: Array<Endereco> = new Array<Endereco>();
	contatos: Array<Contato> = new Array<Contato>();
	tipoContatos: Array<TipoContato> = new Array<TipoContato>();
	ouvidorias: Array<Ouvidoria> = new Array<Ouvidoria>();

	constructor(private enderecoService: EnderecoService,
		        private contatoService: ContatoService,
		        private tipoContatoService: TipoContatoService,
		        private abrirLinkService: AbrirLinkService,
		        private ouvidoriaService: OuvidoriaService,
		        private menuSuperiorService: MenuSuperiorService,
		        private ligarService: LigarService) { }

	ionViewWillEnter() {
		this.menuSuperiorService.setTitle("Contato", "");
		this.menuSuperiorService.showBackButton(true);
	}

	ngOnInit() {
		this.buscarEndereco();
		this.buscarTipoContatos();
		this.buscar();
		this.buscarOuvidoria();
	}

	buscarEndereco() {
		this.enderecoService.buscar().subscribe(enderecosProcon => {
			this.enderecosProcon = enderecosProcon;
		}
			, error => { console.log(error); });
	}

	buscar() {
		this.contatoService.buscar().subscribe(contatos => { this.contatos = contatos }
			, error => { console.log(error) });
	}

	buscarTipoContatos() {
		this.tipoContatoService.buscar().subscribe(tipoContatos => { this.tipoContatos = tipoContatos }
			, error => { console.log(error); });
	}

	abrirLink(Endereco) {
		let link = "https://www.google.com.br/maps/place/" + Endereco.lgdepc + ", " +
			Endereco.numeroepc + "," + Endereco.bairroepc + "," + Endereco.cepepc;
		this.abrirLinkService.abrir(link);
	}

	ligar(contato) {
		this.ligarService.ligar(contato);
	}


	buscarOuvidoria() {
		this.ouvidoriaService.buscar().then((ouvidorias: Array<Ouvidoria>) => {
			ouvidorias.sort((a, b) => {
				let comparison = 0;

				let titleA = a.desorgcon.toUpperCase();
				let titleB = b.desorgcon.toUpperCase();

				if (titleA > titleB)
					comparison = 1;
				else if (titleA < titleB)
					comparison = -1;

				return comparison;
			}).map((ouvidoria, index) => {
				let telMask;
				if (ouvidoria.telorgcon != undefined && ouvidoria.telorgcon.length == 10) {
					telMask = this.maskTelefone(ouvidoria.telorgcon);
					ouvidoria.telorgcon = telMask;
				}
				this.ouvidorias.push(ouvidoria);
			});
		}, (error) => {
			console.log(error);
		});
	}

	maskTelefone(telefone: String) {
		let mask = '('
		mask += telefone.substring(0, 2);
		mask += ') ';
		mask += telefone.substring(2, 6);
		mask += '-'
		mask += telefone.substring(6, 10);
		return mask;
	}
}
