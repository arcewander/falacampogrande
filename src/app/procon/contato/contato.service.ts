import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';

import { Contato } from '../../model/Contato';

import { AmbienteService } from '../../ambiente/ambiente.service';
import { Ambiente } from '../../model/Ambiente';

@Injectable({
  providedIn: 'root'
})
export class ContatoService {

	contatos: Array<Contato> = new Array<Contato>();
	ambiente: Ambiente = new Ambiente();

	constructor(private http: HttpClient, private ambienteService: AmbienteService) {
		
	 }

  	buscar(){
		this.ambiente = this.ambienteService.getAmbiente();
  	  	return this.http.get(this.ambiente.url+"contatoprocon").pipe(
          map(contatos => 
          this.contatos = < Array<Contato> > contatos
        ));    	
		}
		
	
}
