import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';

import { Endereco } from '../../model/Endereco';
import { AmbienteService } from '../../ambiente/ambiente.service';
import { Ambiente } from '../../model/Ambiente';

@Injectable({
  providedIn: 'root'
})
export class EnderecoService {

  private enderecosProcon: Array<Endereco> = new Array<Endereco>();
  ambiente: Ambiente = new Ambiente();

  constructor(private http: HttpClient, private ambienteService: AmbienteService) {

  }

  buscar() {
    this.ambiente = this.ambienteService.getAmbiente();
    return this.http.get(this.ambiente.url + "enderecoprocon/procon").pipe(
      map(enderecosProcon =>
        this.enderecosProcon = <Array<Endereco>>enderecosProcon
      ));
  }

  buscarPor(logradouro) {
    this.ambiente = this.ambienteService.getAmbiente();
    return this.http.get(this.ambiente.url + "endereco/end/" + logradouro).pipe(
      map(enderecosProcon =>
        this.enderecosProcon = <Array<Endereco>>enderecosProcon
      ));
  }


}
