import { Injectable } from '@angular/core';

import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { TipoContato } from '../../model/TipoContato';

import { AmbienteService } from '../../ambiente/ambiente.service';
import { Ambiente } from '../../model/Ambiente';

@Injectable({
  providedIn: 'root'
})
export class TipoContatoService {

  tipoContatos: Array<TipoContato> = new Array<TipoContato>();
  ambiente: Ambiente = new Ambiente();

  constructor(private http: HttpClient, private ambienteService: AmbienteService) { }

  buscar() {
    this.ambienteService.getAmbiente();
    return this.http.get(this.ambiente.url +'tipocontato/tipoContatos').pipe(
        map(tipoContatos =>
          this.tipoContatos = tipoContatos as Array<TipoContato>
        ));
  }

}
