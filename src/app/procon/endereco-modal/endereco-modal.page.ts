import { Component, OnInit } from '@angular/core';

import { ModalController, NavParams, Platform, NavController } from '@ionic/angular';
import { AguardeService } from '../../aguarde/aguarde.service';
import { AlertaService } from '../../alerta/alerta.service';
import { ModalService } from '../../modal-service/modal.service';
import { EnderecoService } from '../endereco/endereco.service';

import { Alerta } from '../../model/Alerta';
import { Endereco } from '../../model/Endereco';

@Component({
  selector: 'app-endereco-modal',
  templateUrl: './endereco-modal.page.html',
  styleUrls: ['./endereco-modal.page.scss'],
})
export class EnderecoModalPage implements OnInit {

  logradouro: String;
  enderecos: Array<Endereco>;
  modal: ModalController;

  constructor(private modalController: ModalController,
              public navParams: NavParams,
              private aguardeService: AguardeService,
              private alertaService: AlertaService,
              private platform: Platform,
              private modalService: ModalService,
              private enderecoService: EnderecoService) { }

  ngOnInit() {
    setTimeout(() => {
      let barSearch = document.getElementById('ion-search');
      barSearch.focus();
    }, 1000);
  }

  buscarPorLogradouro() {

    if (this.logradouro.length > 3) {
      this.enderecoService.buscarPor(this.logradouro).subscribe((enderecos: Array<Endereco>) => {

        this.enderecos = enderecos;
      }
        , error => {
          console.log(error);
        });
    }
  }

  selecionar(endereco) {

    this.modal.dismiss(endereco);
  }

  fecharModal() {
    if (this.modalService.existeModalAberto()) {
      this.modal.dismiss();
    }
  }
}
