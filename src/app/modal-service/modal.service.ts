import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ModalService {

  modal: HTMLIonModalElement;

  constructor() { }


  setModal(modal: HTMLIonModalElement){
    this.modal = modal;
  }

  remover() {
    this.modal = undefined;
  }

  existeModalAberto() {
    return this.modal !== undefined;
  }
}
