import { Injectable } from '@angular/core';
import { Alerta } from '../model/Alerta';
import { AlertaService } from '../alerta/alerta.service';
import { Diagnostic } from '@ionic-native/diagnostic/ngx';

@Injectable({
  providedIn: 'root'
})
export class PermissaoService {

  alerta: Alerta;

  constructor(private diagnostic: Diagnostic, private alertaService: AlertaService) { }

  abrirConfiguracoesDoAPP() {
    return this.diagnostic.switchToSettings();
  }

  ativarLocalizacao() {
    this.diagnostic.switchToLocationSettings();
  }

  cameraAtiva() {
    return this.diagnostic.isCameraAvailable();
  }

  localizacaoAtiva() {
    return this.diagnostic.isLocationEnabled();
  }

  solicitarPermissaoParaLocalizacao() {
    return this.diagnostic.requestRuntimePermission(this.diagnostic.permission.ACCESS_FINE_LOCATION);
  }
}
