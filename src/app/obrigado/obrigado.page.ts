import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams, Platform, NavController } from '@ionic/angular';
import { ViewController } from '@ionic/core';
import { EventEmitter } from 'protractor';
import { ModalService } from '../modal-service/modal.service';


@Component({
  selector: 'app-obrigado',
  templateUrl: './obrigado.page.html',
  styleUrls: ['./obrigado.page.scss'],
})
export class ObrigadoPage implements OnInit {

  tela: string;
  EhProcon: boolean;
  modal:ModalController;

  constructor(private modalController: ModalController,
              public navParams: NavParams,
              private platform: Platform,
              private modalService: ModalService,
              private navCtrl: NavController
    ) {
      this.tela = navParams.get('current');
      this.EhProcon = navParams.get('EhProcon');
    }

  ngOnInit() {}

  acompanhar() {
    this.modal.dismiss();
    this.modalService.remover();
    if (this.EhProcon) {
      this.navCtrl.navigateForward('procon/minhas-solicitacoes');
    } else {
      this.navCtrl.navigateForward('minhas-solicitacoes');
    }
  }

  novaSolicitacao() {
    this.modal.dismiss();
    this.modalService.remover();
		//this.navCtrl.navigateBack(this.tela);
  }

  fecharModal() {
    if (this.modalService.existeModalAberto()) {
      this.modal.dismiss();
      this.modalService.remover();
    }
  }
}
