import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { MascaraModule } from './mascara/mascara.module';
import { MenuSuperiorPageModule } from './menu-superior/menu-superior.module';
import { MenuLateralPageModule } from './menu-lateral/menu-lateral.module';
import { AppVersion } from '@ionic-native/app-version/ngx';

import { CallNumber } from '@ionic-native/call-number/ngx';
import { DatePipe } from '@angular/common';
import { Diagnostic } from '@ionic-native/diagnostic/ngx';

import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { SQLite } from '@ionic-native/sqlite/ngx';


import { HttpClientModule } from '@angular/common/http';
import { AlertaService } from './alerta/alerta.service';
import { AguardeService } from './aguarde/aguarde.service';
import { CadastrarUsuarioService } from './cadastrar-usuario/cadastrar-usuario.service';
import { DatabaseService } from './database/database.service';
import { DataService } from './data/data.service';
import { RedefinirSenhaPage } from './redefinir-senha/redefinir-senha.page';
import { LoginService } from './login/login.service';
import { OrgaoService } from './orgao/orgao.service';
import { MenuLateralService } from './menu-lateral/menu-lateral.service';
import { TelaInicialService } from './tela-inicial/tela-inicial.service';
import { NotificacaoService } from './notificacao/notificacao.service';
import { RamoAtividadeModalPageModule } from './ramo-atividade-modal/ramo-atividade-modal.module';
import { TermoPageModule } from './termo/termo.module';
import { TermoUsoAppPageModule } from './termo-uso-app/termo-uso-app.module';
import { CameraService } from './camera-service/camera-service.service';
import { ArquivoService } from './arquivo-service/arquivo-service.service';
import { AbrirLinkService } from './abrir-link/abrir-link.service';
import { SolicitacaoService } from './solicitacao/solicitacao.service';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { File } from '@ionic-native/file/ngx';
import { FileChooser } from '@ionic-native/file-chooser/ngx';
import { FilePath } from '@ionic-native/file-path/ngx';
import { Camera } from '@ionic-native/camera/ngx';
import { Base64 } from '@ionic-native/base64/ngx';
import { ObrigadoPageModule } from './obrigado/obrigado.module';

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    HttpClientModule,
    MascaraModule,
    MenuLateralPageModule,
    MenuSuperiorPageModule,
    ObrigadoPageModule,
    RamoAtividadeModalPageModule,
    TermoPageModule,
    TermoUsoAppPageModule
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    AppVersion,
    Base64,
    Camera,
    DatePipe,
    Diagnostic,
    File,
    FileChooser,
    FilePath,
    Geolocation,
    InAppBrowser,
    SQLite,
    CallNumber,
    CameraService,
    AbrirLinkService,
    AlertaService,
    AguardeService,
    ArquivoService,
    CadastrarUsuarioService,
    DatabaseService,
    DataService,
    LoginService,
    MenuLateralService,
    NotificacaoService,
    OrgaoService,
    RedefinirSenhaPage,
    SolicitacaoService,
    TelaInicialService,
  ],
  bootstrap: [AppComponent]  ,
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class AppModule {}
