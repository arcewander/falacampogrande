import { Component, OnInit } from '@angular/core';
import { Ambiente } from '../model/Ambiente';
import { AmbienteService } from '../ambiente/ambiente.service';
import { AlertaService } from '../alerta/alerta.service';
import { Alerta } from '../model/Alerta';

@Component({
  selector: 'app-configuracoes',
  templateUrl: './configuracoes.page.html',
  styleUrls: ['./configuracoes.page.scss'],
})
export class ConfiguracoesPage implements OnInit {

  ambiente: Ambiente = new Ambiente();
  ambientes: Array<Ambiente> = new Array<Ambiente>();

  constructor(private ambienteService: AmbienteService,
              private alertaService: AlertaService) {
    this.listar();
    //this.ambienteAtual();
   }

  ngOnInit() { }

  listar() {
    this.ambienteService.listar((ambientes) => {
        this.ambientes = new Array<Ambiente>();
        this.ambientes = ambientes;
    });
  }

  selecionar(ambiente) {
    this.ambienteService.update(ambiente);

    const alerta: Alerta = new Alerta();
    alerta.titulo = 'Sucesso!';
    alerta.subtitulo = '';
    alerta.mensagem = 'Ambiente alterado com sucesso';

    this.alertaService.apresentarAlertaDeSucesso(alerta);
    this.ambienteAtual();
  }

  ambienteAtual() {
    this.ambienteService.ambienteAtual((ambiente) => {
        this.ambiente = ambiente as Ambiente; });
  }
}
