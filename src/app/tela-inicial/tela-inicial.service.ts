import { Injectable } from '@angular/core';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';

@Injectable({
  providedIn: 'root'
})
export class TelaInicialService {

  private idView: string;

  constructor(private iab: InAppBrowser) { }

  scrollIntoView() {
    if (this.idView) {
      document.getElementById(this.idView).scrollIntoView({ behavior : 'smooth'});
    }
  }

  setIdView(id: string) {
    this.idView = id;
  }
}
