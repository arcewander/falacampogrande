import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { TelaInicialPage } from './tela-inicial.page';
import { DataService } from '../data/data.service';
import { MenuSuperiorPageModule } from '../menu-superior/menu-superior.module';

const routes: Routes = [
  {
    path: '',
    component: TelaInicialPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MenuSuperiorPageModule,
    RouterModule.forChild(routes)
  ],
  declarations: [TelaInicialPage],
  providers: [DataService]
})
export class TelaInicialPageModule {}
