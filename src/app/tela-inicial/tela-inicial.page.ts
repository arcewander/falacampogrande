import { Notificacao } from './../model/Notificacao';
import { Menu } from './../model/Menu';
import { NotificacaoService } from './../notificacao/notificacao.service';
import { OrgaoService } from './../orgao/orgao.service';

import { Component, OnInit, SimpleChanges } from '@angular/core';
import { NavController, MenuController } from '@ionic/angular';
import { Orgao } from '../model/Orgao';
import { Usuario } from './../model/Usuario';

import { LoginService } from './../login/login.service';
import { AbrirLinkService } from '../abrir-link/abrir-link.service';
import { MenuSuperiorService } from '../menu-superior/menu-superior.service';
import { MenuLateralService } from '../menu-lateral/menu-lateral.service';

import { TelaInicialService } from './tela-inicial.service';
import { AguardeService } from '../aguarde/aguarde.service';
@Component({
  selector: 'app-tela-inicial',
  templateUrl: './tela-inicial.page.html'
})
export class TelaInicialPage implements OnInit {

  usuario: Usuario;
  menu: Menu;
  orgaos: Array<Orgao> = new Array<Orgao>();
  notificacoes: Array<Notificacao> =  new Array<Notificacao>();

  constructor(
      private navCtrl: NavController,
      private orgaoService: OrgaoService,
      private menuController: MenuController,
      private abrirLinkService: AbrirLinkService,
      private notificacaoService: NotificacaoService,
      private loginService: LoginService,
      private menuSuperiorService: MenuSuperiorService,
      private menuLateralService: MenuLateralService,
      private telaInicialService: TelaInicialService,
      private aguardeService: AguardeService,
    ) {
      menuController.enable(true);
      this.menuSuperiorService.setTitle('Fala Campo Grande');

      // this.aguardeService.processar("Carregando menu ...").then(() =>
      // {
      this.orgaoService.buscar().then((orgaos: Array<Orgao>) => {
        this.orgaos = orgaos;
        this.menuLateralService.atualizaMenu();
        this.aguardeService.encerrar();
      });
      // })
      this.usuarioLogado();

  }

  ionViewWillEnter() {
    this.menuSuperiorService.setTitle('Fala Campo Grande', '');
    this.menuSuperiorService.showBackButton(false);
  }

  ngOnInit() {
    this.notificacaoService.notificacoesRemovidas(notificacoes => {
      this.notificacaoService.deletar(notificacoes);
    });
  }

  atualizar(notificacoes) {
    for (let index = 0; index < notificacoes.length; index++) {
      this.notificacaoService.sincronizar(notificacoes[index]);
    }
  }

  abrirTelaDeSolicitacao(orgao) {
    this.orgaoService.setOrgao(orgao);
    this.navCtrl.navigateForward('solicitacoes');
    this.telaInicialService.setIdView(orgao.iconorg);
  }

  abrirLink(link) {
    this.abrirLinkService.abrir(link);
  }

  abrirTelaDeOuvidoria(id) {
    this.navCtrl.navigateForward('ouvidoria');
    this.telaInicialService.setIdView(id);
  }

  abrirTelaProcon(id) {
    this.navCtrl.navigateForward('procon/tela-inicial-procon');
    this.telaInicialService.setIdView(id);
  }

  async usuarioLogado() {
    this.usuario = await this.loginService.getUsuario();
    if (this.usuario !== undefined) {
      this.notificacaoService.buscar(this.usuario.coduso)
        .subscribe((notificacoes: Array<Notificacao>) => {
           this.notificacoes = notificacoes;
           this.atualizar(notificacoes);
      }, error => {
      });
    }
  }
}
