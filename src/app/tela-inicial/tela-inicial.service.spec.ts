import { TestBed } from '@angular/core/testing';

import { TelaInicialService } from './tela-inicial.service';

describe('TelaInicialService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TelaInicialService = TestBed.get(TelaInicialService);
    expect(service).toBeTruthy();
  });
});
