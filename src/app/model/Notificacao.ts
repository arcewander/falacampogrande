export class Notificacao {
	
	codigo_notificacao: Number;
	titulo: string;
	ordem_servico: Number;
	mensagem: string;
	descricao: string;
	servico: string;
	atendimento: string;
    protocolo: Number;
	descricao_avaliacao: string;
	sitnot: Number;
	dtlnot: Date;
	dtrnot: Date;
	dtsnot: Date;

}