export class RamoAtividade{
    atvram: Boolean;
    codram: Number;
    datalt: Date;
    desram: String;
    nomram: String;
    situacao: String;
    usualt: String;
    visram: String;

    constructor(){}
}