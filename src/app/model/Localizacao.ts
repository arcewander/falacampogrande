export class Localizacao{
	
	latitude:String;
	longitude:String;

	constructor(latitude, longitude){
		this.latitude=latitude;
		this.longitude=longitude;
	}
}