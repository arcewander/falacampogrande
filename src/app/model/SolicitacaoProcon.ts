import { Endereco } from './Endereco';
import { ArquivoProcon } from './ArquivoProcon';

export class SolicitacaoProcon {

	codden: Number;
	codram: Number;
	coduso: Number;
	datalt: Date;
	desden: String;
	epcVO: Endereco = new Endereco();
	estden: String;
	resden: String;
	sitden: Number;
	tipden: Number;
	tpeden: Number;
	anpVOs: Array<ArquivoProcon> = new Array<ArquivoProcon>();
	/*nomArq: String;
	tparq: String;
	arqden: Number;*/

	constructor() {
		this.codram = 0;
		this.sitden = 0;
	}
}
