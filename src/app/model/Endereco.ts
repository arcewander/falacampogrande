export class Endereco {
    atvepc: Boolean;
    bairroepc: String;
    cepepc: String;
    codden: Number;
    codepc: Number;
    comepc: String;
    datalt: Date;
    latepc: String;
    lonepc: String;
    lgdepc: String;
    numeroepc: Number;
    regepc: String;
    tbairrotpc: String;
    usualt: Number;


    constructor() { }
}