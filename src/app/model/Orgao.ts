import { Servico } from './Servico';

export class Orgao {
    codorg: number;
    desorg: string;
    botaoorg: string;
    iconorg: string;
    linkorg: string;
    ordemorg: number;
    linkVos: Array<LinkVOs>;
    servicos: Array<Servico>;

    constructor() {}
}

export class LinkVOs {
    link: string;
    slug: string;
    ordlink: string;
}