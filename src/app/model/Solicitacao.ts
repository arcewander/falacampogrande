import { Servico } from "./Servico";

export class Solicitacao { 
    codsol: Number;
    codoss: Number;
    cepsol: String;
    tplsol: String;
    logsol: String;
    baisol: String;
    numsol: Number;
    txtsol: String;
    datsol: String;
    codmts: Number;
    codtpe: Number;
    desmts: String;
    coduso: Number;
    latsol: String;
    lonsol: String;
    arqsta: Blob;
    nomsta: String;
    sitsol: string;
    destps: string;
    obsoss: string;
    ossVO: Servico;
    tfisol: String;
    dafsol: Date;

    constructor() {
        this.codmts = 1;
        this.codtpe = 5;
        this.sitsol = 'A';
    }
}


/*

txtsol: String = "";
    datsol: Date = new Date();
    codtpe: Number;
    cepsol: String = "";
    dafsol: Date = new Date();
    codtps: Number;
    tfisol: String = "";
    tiposs: Number;
    codmts: 1;
    codtpm: String = "";
    codtpo: Number;
    numsol: Number;
    coduso: Number;
    comsol: String = "";
    ofisol: String = "";
    codate: String = "";
    nprsol: String = "";
    baisol: String = "";
    indsol: String = "";
    ninsol: String = "";
    lonsol: String = "";    
    latsol: String = "";
    degsol: String = "";
    codope: String = "";
    sitsol: String = "";
    drosol: String = "";
    tplsol: String = "";
    codind: String = "";
    regsol: String = "";
    dprsol: String = "";
    dessol: String = "";
    tprsol: String = "";
    obssol: String = "";
    vprsol: String = "";
    vimsol: String = "";
    dinsol: String = "";
    codsol: Number;
    logsol: String = "";
    ordseross:Servico;




*/