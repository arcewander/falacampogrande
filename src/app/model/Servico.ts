export class Servico{
	
    apldentps: String;
    aplelotps: String;
    aplrectps: String;
    aplsertps: String;
    codorg: Number;
    codtps: Number;
    destps: String;
    codoss : String;
    codsol : String;
    paioss : String;
    reloss : String;
    sitoss : String;
    obsoss : String;
    usualt : String;
    datalt : String;
    tiposs : Number;
    seross : String;
    controle : String;

    constructor(){
        
    }
}