import { Orgao } from './Orgao';


export class MenuLateral {
    titulo: string;
    pagina: PaginaMenuLateral[];
    constructor() {}
}

export class PaginaMenuLateral {
    titulo: string;
    url: string;
    icone: string;
    orgao: Orgao;
    ehHyperLink: boolean;
    constructor(titulo: string, url: string, icone: string, orgao?: Orgao, ehHyperLink: boolean = false) {
        this.titulo = titulo;
        this.url = url;
        this.icone = icone;
        this.orgao = orgao;
        this.ehHyperLink = ehHyperLink;
    }
}
