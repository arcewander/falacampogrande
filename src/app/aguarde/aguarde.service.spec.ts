import { TestBed } from '@angular/core/testing';

import { AguardeService } from './aguarde.service';

describe('AguardeService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AguardeService = TestBed.get(AguardeService);
    expect(service).toBeTruthy();
  });
});
