import { Injectable } from '@angular/core';
import { LoadingController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class AguardeService {

  public loading: any;
  constructor(public loadingController: LoadingController) {}

	processar(mensagem: string) {
		return new Promise((resolve, reject) => {
			this.loadingController.create({
				message: mensagem
			}).then(loading => {
			 this.loading = loading;
			 this.loading.present();
			 setTimeout(() => { resolve(); }, 500);
			});
		});
	}
		
	encerrar(){
		if(this.loading){
			this.loading.dismiss();
		}		
	}
}
