import { Injectable } from '@angular/core';
import { Alerta } from '../model/Alerta';
import { NavController, AlertController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class AlertaService {

  alert: any;

  constructor(public alertController: AlertController,
              private navCtrl: NavController) {}

  async apresentarAlertaDeAviso(alerta: Alerta, callback) {

    if (alerta.mensagem !== undefined) {
      this.alert = await this.alertController.create({
        header: 'Aviso',
        subHeader: '',
        message: alerta.mensagem,
        buttons: [{
          text: 'OK',
          role: 'OK',
          handler: () => {
            callback();
          }
        }]
      });

      await this.alert.present();
    }

  }


  async apresentarAlertaDeErro(alerta: Alerta, callback) {

    const alert = await this.alertController.create({
      header: 'Atenção',
      subHeader: '',
      message: alerta.mensagem,
      buttons: [{
        text: 'OK',
        role: 'OK',
        handler: () => {
          callback();
        }
      }]
    });

    await alert.present();
  }

  async apresentarAlertaDeSucesso(alerta: Alerta, callback = () => {}) {
    const alert = await this.alertController.create({
      header: alerta.titulo,
      subHeader: '',
      message: alerta.mensagem,
      backdropDismiss : false,
      buttons: [{
        text : 'OK',
        role : 'OK',
        handler: () => {
          callback();
        }
      }],
    });


    await alert.present();
  }

  async apresentarAlertaDeAcao(alerta: Alerta, callback) {
    if (alerta.mensagem !== undefined) {
      this.alert = await this.alertController.create({
        header: 'Atenção',
        subHeader: '',
        message: alerta.mensagem,
        buttons: [
          {
            text: 'Não',
            role: 'Não',
            handler: () => {}
          },
          {
            text: 'Sim',
            role: 'Sim',
            handler: () => {
              callback();
            }
          }
        ]
      });

      await this.alert.present();
    }
  }

}
