import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { TempoAtendimentoPage } from './tempo-atendimento.page';

const routes: Routes = [
  {
    path: '',
    component: TempoAtendimentoPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [TempoAtendimentoPage]
})
export class TempoAtendimentoPageModule {}
