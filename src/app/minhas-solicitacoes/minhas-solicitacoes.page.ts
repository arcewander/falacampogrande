import { Component, OnInit } from '@angular/core';

import { Usuario } from "../model/Usuario";
import { Solicitacao } from "../model/Solicitacao";
import { MinhasSolicitacoesService } from "./minhas-solicitacoes.service";
import { NavController } from '@ionic/angular';
import { LoginService } from '../login/login.service';

@Component({
	selector: 'app-minhas-solicitacoes',
	templateUrl: './minhas-solicitacoes.page.html',
	styleUrls: ['./minhas-solicitacoes.page.scss'],
})
export class MinhasSolicitacoesPage implements OnInit {

	solicitacoes: Array<Solicitacao>;
	public usuario: Usuario = new Usuario();
	public detalhes: boolean = false;

	constructor(private minhasSolicitacoesService: MinhasSolicitacoesService,
		private loginService: LoginService,
		private navCtrl: NavController) {
		this.usuario = this.loginService.getUsuario();
		this.buscar();
	}

	ngOnInit() { }

	buscar() {
		this.minhasSolicitacoesService.buscar(this.usuario.coduso)
			.subscribe((solicitacoes: Array<Solicitacao>) => { this.solicitacoes = solicitacoes; }
				, error => { console.info(error); });
	}

	mostrarDetalhes(codsol) {
		this.detalhes = codsol;
	}

	esconderDetalhes() {
		this.detalhes = false;
	}

	irParaLegenda(){
		this.navCtrl.navigateForward('legenda');
	}
}
