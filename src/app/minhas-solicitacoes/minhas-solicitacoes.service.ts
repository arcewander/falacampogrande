import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';

import { Solicitacao } from "../model/Solicitacao";

import { AmbienteService } from './../ambiente/ambiente.service';
import { Ambiente } from './../model/Ambiente';
@Injectable({
  providedIn: 'root'
})
export class MinhasSolicitacoesService {

	solicitacoes: Array<Solicitacao> = new Array<Solicitacao>();
	ambiente: Ambiente = new Ambiente();

	constructor(private http: HttpClient, private ambienteService: AmbienteService) {}

  	buscar(coduso) {
		this.ambiente = this.ambienteService.getAmbiente();
   	  
  	  return this.http.get(this.ambiente.url+"solicitacao/solicitacoes/"+coduso).pipe(
          map(solicitacoes => 
          this.solicitacoes = < Array<Solicitacao> > solicitacoes
        ));    	
		}
}
