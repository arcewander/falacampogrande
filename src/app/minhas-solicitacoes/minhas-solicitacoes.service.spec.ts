import { TestBed } from '@angular/core/testing';

import { MinhasSolicitacoesService } from './minhas-solicitacoes.service';

describe('MinhasSolicitacoesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MinhasSolicitacoesService = TestBed.get(MinhasSolicitacoesService);
    expect(service).toBeTruthy();
  });
});
