import { Component, OnInit } from '@angular/core';
;
import { IonIcon } from '@ionic/angular';

@Component({
  selector: 'app-legenda',
  templateUrl: './legenda.page.html',
  styleUrls: ['./legenda.page.scss'],
})
export class LegendaPage implements OnInit {

  abertoConcluido: boolean = true;
  abertoAberta: boolean = true;
  abertoEmAnalise: boolean = true;
  abertoFinalizada: boolean = true;


  constructor() { }

  ngOnInit() {
  }

  mostrarDetalhes(id: string) {
    let content = document.getElementById(id);

    switch (id) {
      case 'detalhesConcluida':
        content.hidden = this.abertoConcluido = !content.hidden;
        break;
      case 'detalhesAberta':
        content.hidden = this.abertoAberta = !content.hidden;
        break;
      case 'detalhesEmAnalise':
        content.hidden = this.abertoEmAnalise = !content.hidden;
        break;
      case 'detalhesFinalizada':
        content.hidden = this.abertoFinalizada = !content.hidden;
        break;
    }
  }
}
