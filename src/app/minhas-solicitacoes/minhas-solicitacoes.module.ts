import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { MinhasSolicitacoesPage } from './minhas-solicitacoes.page';

const routes: Routes = [
  {
    path: '',
    component: MinhasSolicitacoesPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [MinhasSolicitacoesPage]
})
export class MinhasSolicitacoesPageModule {}
