import { Component, OnInit } from '@angular/core';
import { Ouvidoria } from "../model/Ouvidoria";

import { OuvidoriaService } from "./ouvidoria.service";
import { TelaInicialService } from "../tela-inicial/tela-inicial.service";
import { MenuSuperiorService } from '../menu-superior/menu-superior.service';

@Component({
  selector: 'app-ouvidoria',
  templateUrl: './ouvidoria.page.html',
  styleUrls: ['./ouvidoria.page.scss'],
})
export class OuvidoriaPage implements OnInit {

  ouvidoria: Ouvidoria;
  ouvidoriaGeral: Ouvidoria;
  ouvidorias: Array<Ouvidoria> = new Array<Ouvidoria>();

  constructor(
    private ouvidoriaService: OuvidoriaService,
    private menuSuperiorService: MenuSuperiorService,
    private telaInicialService: TelaInicialService) {
    this.buscar();
  }

  ngOnInit() {
    this.menuSuperiorService.setTitle('OUVIDORIA', "");
    this.menuSuperiorService.showBackButton(true);
  }

  ngOnDestroy() {
    this.telaInicialService.scrollIntoView();
  }

  buscar() {
    this.ouvidoriaService.buscar().then((ouvidorias: Array<Ouvidoria>) => {
      ouvidorias.sort((a, b) => {
        let comparison = 0;

        let titleA = a.desorgcon.toUpperCase();
        let titleB = b.desorgcon.toUpperCase();

        if (titleA > titleB)
          comparison = 1;
        else if (titleA < titleB)
          comparison = -1;

        return comparison;
      }).map((ouvidoria, index) => {
        let telMask;
        if (ouvidoria.telorgcon !== undefined && ouvidoria.telorgcon.length === 10) {
          telMask = this.maskTelefone(ouvidoria.telorgcon);
          ouvidoria.telorgcon = telMask;
        }
        if (ouvidoria.codorgcon === 4) {
          this.ouvidoriaGeral = ouvidoria;
        } else {
          this.ouvidorias.push(ouvidoria);
        }
      });
    }, (error) => {
      console.log(error);
    });
  }

  maskTelefone(telefone: String) {
    let mask = '('
    mask += telefone.substring(0, 2);
    mask += ') ';
    mask += telefone.substring(2, 6);
    mask += '-'
    mask += telefone.substring(6, 10);
    return mask;
  }
}
