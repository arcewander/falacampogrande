import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';

import { Ouvidoria } from '../model/Ouvidoria';
import { AmbienteService } from './../ambiente/ambiente.service';
import { Ambiente } from './../model/Ambiente';

@Injectable({
  providedIn: 'root'
})
export class OuvidoriaService {

  ouvidoria: Ouvidoria;
  private ouvidorias: Array<Ouvidoria> = new Array<Ouvidoria>();
  ambiente: Ambiente = new Ambiente();
  constructor(private http: HttpClient, private ambienteService: AmbienteService) {
  }

  buscar() {
    return new Promise((resolve, reject) => {
      this.ambiente = this.ambienteService.getAmbiente();

      this.http.get(this.ambiente.url + 'configapk').pipe(
        map(ouvidorias =>
          this.ouvidorias = <Array<Ouvidoria>>ouvidorias
        )).subscribe((ouvidorias: Array<Ouvidoria>) => {
          resolve(ouvidorias);
        });
    });
  }


}
