import { Component, OnInit } from '@angular/core';
import { Notificacao } from '../model/Notificacao';
import { Usuario } from '../model/Usuario';
import { LoginService } from '../login/login.service';
import { Router } from '@angular/router';
import { NavController, Platform } from '@ionic/angular';
import { MenuSuperiorService } from '../menu-superior/menu-superior.service';
import { NotificacaoService } from './notificacao.service';
import { DatabaseService } from '../database/database.service';

@Component({
  selector: 'app-notificacao',
  templateUrl: './notificacao.page.html',
  styleUrls: ['./notificacao.page.scss'],
})
export class NotificacaoPage implements OnInit {

  usuario: Usuario = new Usuario();
  notificacoes: Array<Notificacao> = new Array<Notificacao>();
  notificacao: Notificacao = new Notificacao();

  public detalhes: Number = 0;
  constructor(private loginService: LoginService,
              private router: Router,
              private navCtrl: NavController,
              private menuSuperiorService: MenuSuperiorService,
              private platform: Platform,
              private notificacaoService: NotificacaoService,
              private databaseService: DatabaseService) {
    this.usuarioLogado();
    this.buscar();
  }

  ngOnInit() {
    this.platform.backButton.subscribe(() => {
      if (this.router.isActive('notificacao', true)) {
        this.goBack();
      }
    });
  }

  buscar() {
    this.notificacaoService.notificacoesNaoLidas(notificacoes => {
      this.notificacoes = notificacoes;
    });
  }

  atualizar() {
    for (let i = 0; i < this.notificacoes.length; i++) {
      this.notificacaoService.sincronizar(this.notificacoes[i]);
    }
  }

  goBack() {
    this.navCtrl.navigateBack('tela-inicial');
    this.menuSuperiorService.atualizaQuantidade();
  }

  lida(notificacao: Notificacao) {
    notificacao.sitnot = 1;
    notificacao.dtlnot = new Date();
    this.databaseService.update(notificacao)
      .then(data => {
        this.notificacaoService.lida(notificacao);
      })
      .catch(error => { console.log(error); });
  }

  usuarioLogado() {
    this.usuario = this.loginService.getUsuario();
  }

  mostrarDetalhes(notificacao: Notificacao) {
    this.detalhes = notificacao.codigo_notificacao;
    this.lida(notificacao);
  }

  esconderDetalhes() {
    this.detalhes = 0;
  }


  deletarDoApp(notificacao) {
    notificacao.sitnot = 2;
    notificacao.dtrnot = new Date();
    this.databaseService.update(notificacao).then((success) => {

      this.notificacaoService.removida(notificacao).subscribe((notificacao: Notificacao) => {
        this.databaseService.delete(notificacao);
      });
    }).catch(error => {
      console.log(JSON.stringify(error));
    });

  }

}
