import { Injectable } from '@angular/core';
import { SQLiteObject } from '@ionic-native/sqlite/ngx';
import { Notificacao } from '../model/Notificacao';
import { Usuario } from '../model/Usuario';
import { Ambiente } from '../model/Ambiente';
import { HttpClient } from '@angular/common/http';
import { DatabaseService } from '../database/database.service';
import { DataService } from '../data/data.service';
import { AmbienteService } from '../ambiente/ambiente.service';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class NotificacaoService {

  notificacoes: Array<Notificacao> = new Array<Notificacao>();
  notificacao: Notificacao = new Notificacao();
  usuario: Usuario = new Usuario();
  ambiente: Ambiente = new Ambiente();

  constructor(private http: HttpClient, private databaseService: DatabaseService,
              private dataService: DataService, private ambienteService: AmbienteService) {}

  buscar(coduso) {
    this.ambiente = this.ambienteService.getAmbiente();
    return this.http.get(this.ambiente.url + 'notificacao/buscar/' + coduso).pipe(
      map((notificacoes: Array<Notificacao>) => {
        return this.notificacoes = notificacoes;
      })
    );
  }

  sincronizar(notificacao: Notificacao) {
    this.ambiente = this.ambienteService.getAmbiente();
    this.buscarNotificacaoPor(notificacao);
    return this.http.get(this.ambiente.url + 'notificacao/atualizar/' + notificacao.codigo_notificacao);
  }

  lida(notificacao: Notificacao) {
    const not: any = { 'codigo_notificacao': notificacao.codigo_notificacao,
			'dtlnot': this.dataService.formatarStringParaData(notificacao.dtlnot),
			'sitnot': 1
		};
		this.ambiente = this.ambienteService.getAmbiente();
		 let request = this.http.post(this.ambiente.url + 'notificacao/lida', not, { headers: { 'Content-Type': 'application/json' } });
		 request.subscribe(() => { });
		  return;
	}

	removida(notificacao: Notificacao) {
		this.ambiente = this.ambienteService.getAmbiente();
		const not: any = {
			'codigo_notificacao': notificacao.codigo_notificacao,
			'dtrnot': this.dataService.formatarStringParaData(notificacao.dtrnot),
			'sitnot': 2
		};
		console.log(not);
		return this.http.post(this.ambiente.url + 'notificacao/removida',
			JSON.stringify(not),
			{ headers: { 'Content-Type': 'application/json' } });
	}

	salvar(notificacao: Notificacao) {
		notificacao.sitnot = notificacao.sitnot == null ? 0 : notificacao.sitnot;
		notificacao.dtsnot = new Date();
		this.databaseService.abrirConexao().then((db: SQLiteObject) => {
			db.executeSql('INSERT INTO notificacao (codigo_notificacao, titulo ,mensagem, descricao, atendimento, sitnot, dtsnot) VALUES(?,?,?,?,?,?,?) ',
				[notificacao.codigo_notificacao, notificacao.titulo, notificacao.mensagem, notificacao.descricao, notificacao.atendimento,
				notificacao.sitnot, notificacao.dtsnot])
				.catch(e => console.log(e));
		}, (error) => { console.log('ERROR: ', error); }
		);
	}

	listar(callback) {
		return this.databaseService.abrirConexao().then((db: SQLiteObject) => {
			db.executeSql('SELECT * FROM notificacao ', [])
				.then(results => {
					if (results.rows.length > 0) {
						for (let i = results.rows.length - 1; i >= 0; i--) {
							this.notificacao = results.rows.item(i);
							this.notificacoes.push(this.notificacao);
						}
						console.log('Lista de Notificações: ' + JSON.stringify(this.notificacoes));

					} else {
						this.notificacoes = new Array<Notificacao>();
					}
					callback(this.notificacoes);
				}).catch(error => { console.log(error); });
		});
	}


	buscarNotificacaoPor(notificacao: Notificacao) {
		let codigo = [notificacao.codigo_notificacao];
		this.databaseService.abrirConexao().then((db: SQLiteObject) => {
			db.executeSql('SELECT * FROM notificacao WHERE codigo_notificacao = ?', codigo).then(results => {
				if (results.rows.length <= 0) {
					this.notificacao = notificacao;
					this.salvar(this.notificacao);
				}
			}).catch(error => { console.log(error); });
		});
	}

	update(notificacao: Notificacao) {
		console.log('UPDATE');
		return this.databaseService.update(notificacao).then(
			(data) => { console.log(data); }
		);
	}

	notificacoesNaoLidas(callback) {
		let data: any = [2];
		this.databaseService.abrirConexao().then((db: SQLiteObject) => {
			db.executeSql('SELECT * FROM notificacao WHERE sitnot != ? ORDER BY dtsnot DESC', data).then(results => {
				for (var i = 0; i < results.rows.length; i++) {
					this.notificacao = results.rows.item(i);

					if (this.notificacoes.find(x => x.codigo_notificacao
						== this.notificacao.codigo_notificacao)) {
						let index = this.notificacoes.findIndex(x => x.codigo_notificacao
							== this.notificacao.codigo_notificacao);

						this.notificacoes.splice(index, 1);

						this.notificacoes.push(this.notificacao);
					}
				}
				callback(this.notificacoes);

			}).catch((error) => { });
		});
	}

	contarNotificacoesNaoLidas(callback) {
		let data: any = [0];

		this.databaseService.abrirConexao().then((db: SQLiteObject) => {
			db.executeSql('SELECT COUNT(*) as quantidade FROM notificacao WHERE sitnot = ?', data).then(results => {
				callback(results.rows.item(0).quantidade);
			});
		});



	}

	notificacoesRemovidas(callback) {
		let data: any = [2];
		let not = [];
		this.databaseService.abrirConexao().then((db: SQLiteObject) => {
			db.executeSql('SELECT * FROM notificacao WHERE sitnot =  ?', data).then(results => {

				for (let i = 0; i < results.rows.length; i++) {
					this.notificacao = results.rows.item(i);
					not.push(this.notificacao);
				}

				callback(not);

			}).catch((error) => { });
		});
	}

	deletar(notificacoes) {
		notificacoes.forEach(notificacao => {
			this.databaseService.delete(notificacao);
		});
	}

	remove() {
		return this.databaseService.abrirConexao()
			.then((db: SQLiteObject) => {

				return db.executeSql('DELETE FROM notificacao')
					.catch((e) => console.error(e));
			})
			.catch((e) => console.error(e));
	}

}
