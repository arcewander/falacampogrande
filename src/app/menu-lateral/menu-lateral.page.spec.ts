import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuLateralPage } from './menu-lateral.page';

describe('MenuLateralPage', () => {
  let component: MenuLateralPage;
  let fixture: ComponentFixture<MenuLateralPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenuLateralPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuLateralPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
