import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { MenuLateralPage } from './menu-lateral.page';

const routes: Routes = [
  {
    path: '',
    component: MenuLateralPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule
  ],
  declarations: [MenuLateralPage],
  exports: [
    MenuLateralPage
  ]
})
export class MenuLateralPageModule {}
