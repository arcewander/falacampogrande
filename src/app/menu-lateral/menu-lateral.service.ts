import { Injectable } from '@angular/core';
import { PaginaMenuLateral, MenuLateral } from '../model/MenuLateral';
import { OrgaoService } from '../orgao/orgao.service';

@Injectable({
  providedIn: 'root'
})
export class MenuLateralService {

  private itens = new Array<MenuLateral>();

  constructor(private orgaoService: OrgaoService) { }

  criarNavegacao(menu: MenuLateral) {
    menu.titulo = 'Navegação';
    menu.pagina = new Array<PaginaMenuLateral>();

    menu.pagina.push(new PaginaMenuLateral(
      'Home',
      '/tela-inicial',
      'home'
    ));

    this.itens.push(menu);
  }

  criarSolicitacoes(menu: MenuLateral) {
    menu.titulo = 'Solicitações';
    menu.pagina = new Array<PaginaMenuLateral>();

    menu.pagina.push(new PaginaMenuLateral(
      'Meus Protocolos',
      '/minhas-solicitacoes',
      'list'
    ));

    menu.pagina.push(new PaginaMenuLateral(
      'Meus Protocolos PROCON',
      '/procon/minhas-solicitacoes',
      'list'
    ));

    this.itens.push(menu);
  }

  criarNotificacao(menu: MenuLateral) {
    menu.titulo = 'Notificações';
    menu.pagina = new Array<PaginaMenuLateral>();

    menu.pagina.push(new PaginaMenuLateral(
      'Minhas Notificações',
      '/notificacao',
      'notifications'
    ));

    this.itens.push(menu);
  }

  criarConfiguracao(menu: MenuLateral) {
    menu.titulo = 'Configuração';
    menu.pagina = new Array<PaginaMenuLateral>();

    menu.pagina.push(new PaginaMenuLateral(
      'Perfil',
      '/perfil',
      'person'
    ));

    menu.pagina.push(new PaginaMenuLateral(
      'Sobre',
      '/sobre',
      'help'
    ));

    menu.pagina.push(new PaginaMenuLateral(
      'Sair',
      '/logout',
      'exit'
    ));

    this.itens.push(menu);
  }

  criarIconeCampoGrande(menu: MenuLateral) {
    menu.titulo = '';
    menu.pagina = new Array<PaginaMenuLateral>();

    menu.pagina.push(new PaginaMenuLateral('', '', 'assets/imagelogo_pmcg.png'));

    this.itens.push(menu);
  }

  atualizaMenu() {
    //console.log(this.itens);
    //if (this.itens.length > 0 ) {return; }

    this.criarNavegacao(new MenuLateral());
    this.criarNotificacao(new MenuLateral());
    this.criarSolicitacoes(new MenuLateral());
    this.criarConfiguracao(new MenuLateral());

    const orgaos = this.orgaoService.getOrgaos();
    const navegacao = this.itens.filter(obj => obj.titulo === 'Navegação')[0];
    for(let i = 0; i < orgaos.length; i++) {
        navegacao.pagina.push(new PaginaMenuLateral(
          orgaos[i].botaoorg,
          orgaos[i].linkorg,
          orgaos[i].iconorg,
          orgaos[i],
          (orgaos[i].linkorg == null || orgaos[i].linkorg === '' ? false : true)));
      }
      console.log(this.itens);
  }

  recuperaInformacoesMenu() {
    return this.itens;
  }

}
