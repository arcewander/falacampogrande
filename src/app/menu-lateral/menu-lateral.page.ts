import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { MenuLateralService } from './menu-lateral.service';
import { AbrirLinkService } from '../abrir-link/abrir-link.service';
import { OrgaoService } from '../orgao/orgao.service';
import { MenuSuperiorService } from '../menu-superior/menu-superior.service';

@Component({
  selector: 'menu-lateral',
  templateUrl: './menu-lateral.page.html',
  styleUrls: ['./menu-lateral.page.scss'],
})
export class MenuLateralPage implements OnInit {

  public itens: any;
  quantidade: any = '0';
  constructor(
    private navCtrl: NavController,
    private menuLateralService: MenuLateralService,
    private abrirLinkService: AbrirLinkService,
    private orgaoService: OrgaoService,
    private menuSuperiorService: MenuSuperiorService
  ) {
    this.itens = this.menuLateralService.recuperaInformacoesMenu();
  }

  ngOnInit() { }

  abrirLink(link) {
    this.abrirLinkService.abrir(link);
  }

  async abrirTelaDeSolicitacao(orgao) {
    this.orgaoService.setOrgao(orgao);
    await this.navCtrl.navigateBack('/tela-inicial', {animated: false});
    await this.navCtrl.navigateForward('solicitacoes');
  }

  async abrirMinhasNotificacoes() {
    await this.navCtrl.navigateBack('/tela-inicial', {animated: false});
    await this.navCtrl.navigateForward('notificacao');

  }

  async AbrirPagina(url) {
    await this.navCtrl.navigateBack('/tela-inicial', {animated: false});
    await this.navCtrl.navigateForward(url);
  }

  async abrirTelaDeOuvidoria() {
    await this.navCtrl.navigateBack('/tela-inicial', {animated: false});
    await this.navCtrl.navigateForward('ouvidoria');
  }

  async abrirTelaProcon() {
    await this.navCtrl.navigateBack('/tela-inicial', {animated: false});
    await this.navCtrl.navigateForward('procon/tela-inicial-procon');
  }
}
