import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RamoAtividadeModalPage } from './ramo-atividade-modal.page';

describe('RamoAtividadeModalPage', () => {
  let component: RamoAtividadeModalPage;
  let fixture: ComponentFixture<RamoAtividadeModalPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RamoAtividadeModalPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RamoAtividadeModalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
