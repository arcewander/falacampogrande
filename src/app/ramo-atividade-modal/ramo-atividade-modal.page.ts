import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams, Platform } from '@ionic/angular';
import { AguardeService } from '../aguarde/aguarde.service';
import { AlertaService } from '../alerta/alerta.service';
import { ModalService } from '../modal-service/modal.service';

@Component({
  selector: 'app-ramo-atividade-modal',
  templateUrl: './ramo-atividade-modal.page.html',
  styleUrls: ['./ramo-atividade-modal.page.scss'],
})
export class RamoAtividadeModalPage implements OnInit {

  textoDigitado: String;
  lista: Array<any>;
  listaFiltrada: Array<any>;
  modal: ModalController;

  constructor(
    private modalController: ModalController,
    public navParams: NavParams,
    private aguardeService: AguardeService,
    private alertaService: AlertaService,
    private platform: Platform,
    private modalService: ModalService
  ) {
    this.aguardeService.processar('').then(() => {
      this.listaFiltrada = <Array<any>> navParams.get('lista'); 
      this.aguardeService.encerrar();
    });
  }

  ngOnInit() { }

  back() {
    this.modal.dismiss();
  }

  filtrar() {
    console.log(this.listaFiltrada);
    this.listaFiltrada = this.lista.filter( 
      item => item.nomram.toString().toLowerCase().indexOf(this.textoDigitado.toString().toLowerCase()) >= 0
    );
  }

  selecionar(item) {
    this.modal.dismiss(item);
  }

  fecharModal() {
    if (this.modalService.existeModalAberto()) {
      this.modal.dismiss();
    }
  }

}
