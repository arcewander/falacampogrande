import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { RamoAtividadeModalPage } from './ramo-atividade-modal.page';

const routes: Routes = [
  {
    path: '',
    component: RamoAtividadeModalPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [RamoAtividadeModalPage]
})
export class RamoAtividadeModalPageModule {}
