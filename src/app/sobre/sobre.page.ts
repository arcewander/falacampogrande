import { Component, OnInit } from '@angular/core';
import { ModalService } from '../modal-service/modal.service';
import { Platform, ModalController } from '@ionic/angular';
import { AppVersion } from '@ionic-native/app-version/ngx';
import { TermoUsoAppPage } from '../termo-uso-app/termo-uso-app.page';

@Component({
  selector: 'app-sobre',
  templateUrl: './sobre.page.html',
  styleUrls: ['./sobre.page.scss'],
})
export class SobrePage implements OnInit {

  ngOnInit(): void {
  }

  versao: any;
  nome: string;
  modal: HTMLIonModalElement;

  constructor(private appVersion: AppVersion,
    public modalController: ModalController,
    private modalService: ModalService,
    private platform: Platform) {
    this.versao = "0.0.0";
    if (platform.is('ios') || platform.is('android')) {
      this.getVersao();
    }
  }

  getVersao() {
    this.appVersion.getVersionNumber().then((version) => {
      this.versao = version;
    });
  }

  async IrParaTermoDeUso() {
    this.modal = await this.modalController.create({
      component: TermoUsoAppPage,
    });
    this.modalService.setModal(this.modal);
    return await this.modal.present();
  }


}
