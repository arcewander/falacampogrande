import { TestBed } from '@angular/core/testing';

import { RedefinirSenhaService } from './redefinir-senha.service';

describe('RedefinirSenhaService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RedefinirSenhaService = TestBed.get(RedefinirSenhaService);
    expect(service).toBeTruthy();
  });
});
