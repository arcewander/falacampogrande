import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { RedefinirSenhaPage } from './redefinir-senha.page';
import { MascaraModule } from '../mascara/mascara.module';

const routes: Routes = [
  {
    path: '',
    component: RedefinirSenhaPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    MascaraModule,

  ],
  declarations: [RedefinirSenhaPage]
})
export class RedefinirSenhaPageModule {}
