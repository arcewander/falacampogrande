import { Component, OnInit } from '@angular/core';
import { Alerta } from '../model/Alerta';
import { NavController } from '@ionic/angular';
import { LoginService } from '../login/login.service';
import { AlertaService } from '../alerta/alerta.service';
import { AguardeService } from '../aguarde/aguarde.service';
import { DataService } from '../data/data.service';
import { Usuario } from '../model/Usuario';
import { RedefinirSenhaService } from './redefinir-senha.service';

@Component({
  selector: 'app-redefinir-senha',
  templateUrl: './redefinir-senha.page.html',
  styleUrls: ['./redefinir-senha.page.scss'],
})
export class RedefinirSenhaPage implements OnInit {

  message = '';
  usuario: Usuario;
  alerta: Alerta;
  PESSOA_FISICA = 'F';

  constructor(
    private navCtrl: NavController,
    private redefinirSenhaService: RedefinirSenhaService,
    private loginService: LoginService,
    private alertaService: AlertaService,
    private aguardeService: AguardeService,
    private dataService: DataService
  ) {
    this.usuario = loginService.getUsuario();
    this.usuario.tpeuso = this.PESSOA_FISICA;
  }

  ngOnInit() { }

  redefinirSenha() {
    this.aguardeService.processar('').then(() => {
      this.usuario.nasuso = this.dataService.formatarStringParaData(this.usuario.nasuso);
      this.usuario.senuso = null;
      this.usuario.nomuso = null;

      this.redefinirSenhaService.redefinirSenha(this.usuario)
      .subscribe( (response) => {
        this.alerta =  response.body as Alerta;
        this.loginService.setUsuario(this.usuario);
        this.aguardeService.encerrar();

        this.alerta.mensagem += ' : ' + this.usuario.cpfuso;
        this.alertaService.apresentarAlertaDeSucesso(this.alerta);
        this.irParaLogin();
      }, error => {
        this.alerta = error.error as Alerta;
        this.aguardeService.encerrar();
        if (error.status === 500) {
          this.alerta.mensagem = 'Houve um erro de comunicação com o servidor. Contate o Administrador do sistema!';
          this.alertaService.apresentarAlertaDeErro(this.alerta, () => { });
        } else {
          this.alertaService.apresentarAlertaDeAviso(this.alerta, () => { });
        }
      });
    });
  }

  irParaLogin() {
    this.navCtrl.navigateRoot('login');
  }
}
