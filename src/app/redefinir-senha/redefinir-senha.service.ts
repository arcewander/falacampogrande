import { Injectable } from '@angular/core';
import { Usuario } from '../model/Usuario';
import { Ambiente } from '../model/Ambiente';
import { HttpClient } from '@angular/common/http';
import { AmbienteService } from '../ambiente/ambiente.service';

@Injectable({
  providedIn: 'root'
})
export class RedefinirSenhaService {

  usuario: Usuario;
  ambiente: Ambiente = new Ambiente();

  constructor(private http: HttpClient, private ambienteService: AmbienteService) { }

  redefinirSenha(usuario: Usuario){
    this.ambiente = this.ambienteService.getAmbiente();
    console.log(JSON.stringify(usuario));
    return this.http.post( this.ambiente.url + 'usuariosol/recuperarsenha',
                          usuario, { headers: {'Content-Type': 'application/json'}, observe: 'response'  } );
  }
}
