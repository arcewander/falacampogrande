import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TermoUsoAppPage } from './termo-uso-app.page';

describe('TermoUsoAppPage', () => {
  let component: TermoUsoAppPage;
  let fixture: ComponentFixture<TermoUsoAppPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TermoUsoAppPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TermoUsoAppPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
