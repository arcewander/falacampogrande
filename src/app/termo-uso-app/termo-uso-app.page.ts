import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-termo-uso-app',
  templateUrl: './termo-uso-app.page.html',
  styleUrls: ['./termo-uso-app.page.scss'],
})
export class TermoUsoAppPage implements OnInit {

  modal: ModalController;
  constructor() { }

  ngOnInit() {
  }

  fecharModal() {
		this.modal.dismiss();
	}

}
