import { Injectable } from '@angular/core';
import { DatePipe } from '@angular/common';
import * as moment from 'moment-timezone';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private datePipe: DatePipe){}

  formatar(data){
    try{
      return moment(data.day.value+"/"+data.month.value+"/"+data.year.value,"DD/MM/YYYY").format("DD/MM/YYYY");
    }catch(error){
      return data;
    }
  }

  formatarStringParaData(string){
    return moment(string,"YYYY-MM-DD").format("DD/MM/YYYY"); 
  }

  formatDateDatabase(data){

    return this.datePipe.transform(data, 'YYYY-MM-DD');
  }

  getMeses(){
    return ["Janeiro", "Fevereiro", "Mar\u00e7o", "Abril", "Maio", "Junho", "Julho", "Agosto","Setembro", "Outubro", "Novembro", "Dezembro" ];
  }

  getMesesAbreviados(){
    return ["Jan", "Fev", "Mar","Abr","Mai","Jun","Jul","Ago","Set","Out","Nov","Dez"];
  }

  getDiasDaSemana(){
    return ["Domingo", "Segunda-Feira", "Ter\u00e7a-Feira","Quarta-Feira","Quinta-Feira","Sexta-Feira", "Sábado" ];
  }

  getDiasDaSemanaAbreviados(){
    return ["dom", "seg", "ter","qua", "qui", "sex", "sab" ];
  }
}
