import { Component, OnInit } from '@angular/core';
import { Usuario } from '../model/Usuario';
import { NavController } from '@ionic/angular';
import { MenuSuperiorService } from '../menu-superior/menu-superior.service';
import { LoginService } from '../login/login.service';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.page.html',
  styleUrls: ['./perfil.page.scss'],
})
export class PerfilPage implements OnInit {

  usuario: Usuario = new Usuario();
  constructor(private loginService: LoginService,
              private NavCtrl: NavController,
              private menuSuperiorService: MenuSuperiorService) {

    this.menuSuperiorService.setTitle('Perfil');

    this.usuario = new Usuario();
    this.getUsuario();
  }

  ngOnInit() {
    this.getUsuario();
  }

  getUsuario() {
    this.usuario = this.loginService.getUsuario();
  }

  IrParaRedefinirSenha() {
    this.NavCtrl.navigateForward('alterar-senha');
  }

}
