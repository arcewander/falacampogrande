import { TestBed } from '@angular/core/testing';

import { AbrirLinkService } from './abrir-link.service';

describe('AbrirLinkService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AbrirLinkService = TestBed.get(AbrirLinkService);
    expect(service).toBeTruthy();
  });
});
