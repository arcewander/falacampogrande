import { Injectable } from '@angular/core';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';

@Injectable({
  providedIn: 'root'
})
export class AbrirLinkService {

  constructor(private iab: InAppBrowser) { }

  abrir(link) {
    const browser = this.iab.create(link);
    browser.on('loadstop').subscribe(event => {
      browser.insertCSS({ code: 'body{color: red;' });
    });
  }
}