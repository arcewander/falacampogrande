import { Injectable } from '@angular/core';
import { Usuario } from '../model/Usuario';
import { HttpClient } from '@angular/common/http';
import { Ambiente } from '../model/Ambiente';
import { AmbienteService } from '../ambiente/ambiente.service';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CadastrarUsuarioService {

  usuario: Usuario = new Usuario();
  ambiente: Ambiente = new Ambiente();

  constructor(private http: HttpClient, private ambienteService: AmbienteService) { }

  cadastrarUsuario(usuario: Usuario) {

    this.ambiente = this.ambienteService.getAmbiente();
    return this.http.post(this.ambiente.url + 'usuariosol/incluir',
      usuario,
      { headers: { 'Content-Type': 'application/json' } })
      .pipe(map(json => {
        var obj = <any> json;
        if (obj.response == 'Cadastro já existente!') {
          return null;
        } else {
          return this.usuario = <Usuario>usuario
        }
      }
      ));

  }

}
