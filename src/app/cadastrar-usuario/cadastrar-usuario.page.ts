import { Component, OnInit } from '@angular/core';
import { Usuario } from '../model/Usuario';
import { NavController, Platform, ModalController, MenuController } from '@ionic/angular';
import { CadastrarUsuarioService } from './cadastrar-usuario.service';
import { AlertaService } from '../alerta/alerta.service';
import { Router } from '@angular/router';
import { LoginService } from '../login/login.service';
import { AguardeService } from '../aguarde/aguarde.service';
import { Alerta } from '../model/Alerta';
import { DataService } from '../data/data.service';
import { ModalService } from '../modal-service/modal.service';

@Component({
  selector: 'app-cadastrar-usuario',
  templateUrl: './cadastrar-usuario.page.html',
  styleUrls: ['./cadastrar-usuario.page.scss'],
})
export class CadastrarUsuarioPage implements OnInit {

  usuario: Usuario = new Usuario();
  PESSOA_FISICA = 'F';

  passwordType = 'password';
  passwordShown = false;

  constructor(private navCtrl: NavController, private cadastrarUsuarioService: CadastrarUsuarioService,
              private alertaService: AlertaService,
              private platform: Platform,
              private router: Router,
              public modalController: ModalController,
              private modalService: ModalService,
              private menuController: MenuController,
              private loginService: LoginService,
              private aguardeService: AguardeService,
              private dataService: DataService,
    ) { }

  ngOnInit() {
  }

  public togglePassword() {
    if (this.passwordShown) {
      this.passwordShown = false;
      this.passwordType = 'password';
    } else {
      this.passwordShown = true;
      this.passwordType = 'text';
    }
  }

  verificaSeEstaVazio(obj) {
    return obj === undefined || obj === '';
  }

  validaCampos() {
    const alerta: Alerta = new Alerta();

    if (this.verificaSeEstaVazio(this.usuario.nomuso)) {
      alerta.mensagem = 'O campo nome é obrigatório';
      alerta.titulo = 'Alerta';
      this.alertaService.apresentarAlertaDeAviso(alerta, () => { });
      return false;
    }
    if (this.verificaSeEstaVazio(this.usuario.nasuso)) {
      alerta.mensagem = 'O campo data nascimento é obrigatório';
      alerta.titulo = 'Alerta';
      this.alertaService.apresentarAlertaDeAviso(alerta, () => { });
      return false;
    }
    if (this.verificaSeEstaVazio(this.usuario.cpfuso)) {
      alerta.mensagem = 'O campo cpf é obrigatório';
      alerta.titulo = 'Alerta';
      this.alertaService.apresentarAlertaDeAviso(alerta, () => { });
      return false;
    }
    if (this.verificaSeEstaVazio(this.usuario.celuso)) {
      alerta.mensagem = 'O campo celular é obrigatório';
      alerta.titulo = 'Alerta';
      this.alertaService.apresentarAlertaDeAviso(alerta, () => { });
      return false;
    }
    if (this.verificaSeEstaVazio(this.usuario.emailuso)) {
      alerta.mensagem = 'O campo email é obrigatório';
      alerta.titulo = 'Alerta';
      this.alertaService.apresentarAlertaDeAviso(alerta, () => { });
      return false;
    }
    if (this.verificaSeEstaVazio(this.usuario.senuso)) {
      alerta.mensagem = 'O campo senha é obrigatório';
      alerta.titulo = 'Alerta';
      this.alertaService.apresentarAlertaDeAviso(alerta, () => { });
      return false;
    }
    if (this.verificaSeEstaVazio(this.usuario.loguso)) {
      alerta.mensagem = 'O campo logradouro é obrigatório';
      alerta.titulo = 'Alerta';
      this.alertaService.apresentarAlertaDeAviso(alerta, () => { });
      return false;
    }
    if (this.verificaSeEstaVazio(this.usuario.numuso)) {
      alerta.mensagem = 'O campo número é obrigatório';
      alerta.titulo = 'Alerta';
      this.alertaService.apresentarAlertaDeAviso(alerta, () => { });
      return false;
    }
    if (this.verificaSeEstaVazio(this.usuario.baiuso)) {
      alerta.mensagem = 'O campo bairro é obrigatório';
      alerta.titulo = 'Alerta';
      this.alertaService.apresentarAlertaDeAviso(alerta, () => { });
      return false;
    }
    if (this.verificaSeEstaVazio(this.usuario.cepuso)) {
      alerta.mensagem = 'O campo CEP é obrigatório';
      alerta.titulo = 'Alerta';
      this.alertaService.apresentarAlertaDeAviso(alerta, () => { });
      return false;
    }

    const dataAtual = new Date(Date.now());
    const dataNascimento = new Date(this.usuario.nasuso);
    if (dataNascimento > dataAtual) {
      alerta.mensagem = 'A data de nascimento não pode ser maior que a data atual.';
      alerta.titulo = 'Alerta';
      this.alertaService.apresentarAlertaDeAviso(alerta, () => { });
      return false;
    } else if (!this.validaCpf(this.usuario.cpfuso)) {
      alerta.mensagem = 'CPF invalido.';
      alerta.titulo = 'Alerta';
      this.alertaService.apresentarAlertaDeAviso(alerta, () => { });
      return false;
    }
    return true;
  }

  cadastrarUsuario() {
    let valido = this.validaCampos();
    if (!valido) return;

    const alerta: Alerta = new Alerta();
    this.aguardeService.processar('').then(() => {

      this.usuario.codtpu = 4;
      this.usuario.tpeuso = this.PESSOA_FISICA;
      this.usuario.nasuso = this.dataService.formatarStringParaData(this.usuario.nasuso);
      this.usuario.emauso = this.usuario.emailuso;

      this.cadastrarUsuarioService.cadastrarUsuario(this.usuario).subscribe(usuario => {
        this.aguardeService.encerrar();

        if (usuario == null) {
          alerta.titulo = this.usuario.nomuso;
          alerta.mensagem = 'Usuário já cadastrado!';

          this.alertaService.apresentarAlertaDeSucesso(alerta);
          return;
        }

        this.usuario.coduso = usuario.coduso;
        alerta.titulo = this.usuario.nomuso;
        alerta.mensagem = 'Usuário cadastrado com sucesso!';

        this.alertaService.apresentarAlertaDeSucesso(alerta,
          () => {

            this.loginService.login(this.usuario).subscribe(( usu: Usuario) => {

              const user = usu;
              // this.usuario = usuario;
              // this.usuario.senuso = senha;
              // this.usuario.cpfuso = usuario.cpfuso;
              this.loginService.setUsuario(user);
              this.loginService.salvar(user);

              this.aguardeService.encerrar();
              this.menuController.enable(true);
              this.irTelaPrincipal();
            });
            //this.navCtrl.back();
          });
      },
        error => {
          this.aguardeService.encerrar();

          alerta.mensagem = 'Não foi possível efutar o seu cadastro. Tente novamente!';
          this.alertaService.apresentarAlertaDeAviso(alerta, () => { });
        });

    });
  }

  irTelaPrincipal() {
    this.navCtrl.navigateRoot('/tela-inicial');
  }

  validaCpf(strCPF: string) {
    strCPF = strCPF.replace('.', '');
    strCPF = strCPF.replace('.', '');
    strCPF = strCPF.replace('-', '');
    let Soma;
    let Resto: number;
    let i;
    Soma = 0;
    if (strCPF === '00000000000') { return false; }
    if (strCPF === '11111111111') {return false; }
    if (strCPF === '22222222222') {return false; }
    if (strCPF === '33333333333') {return false; }
    if (strCPF === '44444444444') {return false; }
    if (strCPF === '55555555555') {return false; }
    if (strCPF === '66666666666') {return false; }
    if (strCPF === '77777777777') {return false; }
    if (strCPF === '88888888888') {return false; }
    if (strCPF === '99999999999')  {return false; }

    for (i = 1; i <= 9; i++) { Soma = Soma + parseInt(strCPF.substring(i - 1, i)) * (11 - i); }
    Resto = (Soma * 10) % 11;

    if ((Resto === 10) || (Resto === 11)) {Resto = 0; }
    if (Resto !== parseInt(strCPF.substring(9, 10))) {return false; }

    Soma = 0;
    for (i = 1; i <= 10; i++) {Soma = Soma + parseInt(strCPF.substring(i - 1, i)) * (12 - i); }
    Resto = (Soma * 10) % 11;

    if ((Resto === 10) || (Resto === 11)) {Resto = 0; }
    if (Resto !== parseInt(strCPF.substring(10, 11))) { return false; }
    return true;
  }

}
