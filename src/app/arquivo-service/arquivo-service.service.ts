import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { FileChooser } from '@ionic-native/file-chooser/ngx';
import { FilePath } from '@ionic-native/file-path/ngx';
import { File, Entry } from '@ionic-native/file/ngx';
import { Base64 } from '@ionic-native/base64/ngx';

import { Solicitacao } from '../model/Solicitacao';
import { Arquivo } from '../model/Arquivo';


@Injectable({
  providedIn: 'root'
})
export class ArquivoService {

  arquivo: Arquivo = new Arquivo();
  solicitacao: Solicitacao = new Solicitacao();

  constructor(
    private http: HttpClient,
    private fileChooser: FileChooser,
    private filePath: FilePath,
    private file: File,
    private base64: Base64) { }

  selecionarArquivoAndroid() {
    return new Promise((resolve, reject) => {

      this.fileChooser.open().then((caminho) => {

        this.filePath.resolveNativePath(caminho)
          .then(caminhoNativo => {

            this.carregarArquivo(caminhoNativo)
              .then(arquivo => { resolve(arquivo); })
              .catch(error => { reject(error); });

          });

      }).catch(error => reject(error));
    });

  }

  getArquivo(caminhoNativo) {
    return new Promise((resolve, reject) => {
      this.file.resolveLocalFilesystemUrl(caminhoNativo)
        .then(arquivo => { resolve(arquivo); })
        .catch(error => reject(error));
    });
  }

  carregarArquivo(caminhoNativo) {
    return new Promise((resolve, reject) => {
      this.getArquivo(caminhoNativo)
        .then((arquivo: Entry) => {

          this.arquivo = new Arquivo();
          let formatoDoArquivo = this.getFormatoDo(arquivo);

          if (formatoDoArquivo === undefined) {
            reject('Formato do arquivo é inválido');
          }

          let diretorio = this.getDiretorioDo(arquivo);

          this.converterArquivoEmByteArray(diretorio, arquivo.name)
            .then((byteArray: Blob) => {

              this.arquivo.arquivo = new Blob([byteArray], { type: formatoDoArquivo });

              let tamanho = this.arquivo.arquivo.size;
              if (tamanho > 25728640) return reject('O arquivo não pode ter mais de 15mb');

              this.arquivo.nome = arquivo.name;
              this.arquivo.tipo = formatoDoArquivo;
              this.arquivo.formato = formatoDoArquivo;

              this.converterArquivoEmBase64(diretorio, arquivo)
                .then(imageBase64 => {
                  const t = imageBase64.toString().split(',')[0].length;
                  this.arquivo.arquivoUrl = imageBase64.toString().substring(t + 1);

                  //console.log(JSON.stringify(this.arquivo));
                  resolve(this.arquivo);

                }).catch(error => reject(error));
            });

        }).catch(error => {
          console.log(error);
          reject(error);
        });
    });
  }

  converterArquivoEmByteArray(diretorio, nomeDoArquivo) {
    return new Promise((resolve, reject) => {
      this.file.readAsArrayBuffer(diretorio, nomeDoArquivo)
        .then(buffer => { resolve(buffer); })
        .catch(error => reject(error));
    });
  }

  converterArquivoEmBase64(diretorio, arquivo: Entry) {
    return new Promise((resolve, reject) => {

      const extensao = arquivo.name.substring(arquivo.name.indexOf('.'));
      this.file.readAsDataURL(diretorio, arquivo.name).then(imageUrl => {

        // if (extensao == '.jpg')
        //   imageUrl = imageUrl.toString().replace('data:image/jpeg;base64,', '');
        // if (extensao == '.png')
        //   imageUrl = imageUrl.toString().replace('data:image/png;base64,', '');
        // if (extensao == '.pdf')
        //   imageUrl = imageUrl.toString().replace('data:image/png;base64,', '');

        resolve(imageUrl);
      }).catch(error => {
        console.log(error);
        reject(error);
      });


    });

  }

  getFormatoDo(arquivo: Entry) {
    const extensao = arquivo.name.split('.').pop();

    if (extensao === 'png' || extensao === 'jpg' || extensao === 'jpeg' || extensao == 'gif') {
      return 'application/' + extensao;

    } else if (extensao === 'doc') {
      return 'application/msword';

    } else if (extensao === 'docx') {
      return 'application/vnd.openxmlformats-officedocument.wordprocessingml.document';

    } else if (extensao === 'pdf') {
      return 'application/pdf';

    } else if (extensao === 'svg') {
      return 'image/svg+xml';

    } else if (extensao === 'xls') {
      return 'application/vnd.ms-excel';

    } else if (extensao == 'xlsx') {
      return 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';

    } else if (extensao === 'rar') {
      return 'application/x-rar-compressed';

    } else if (extensao === 'zip') {
      return 'application/zip';

    } else if (extensao === 'txt') {
      return 'text/plain';

    } else if (extensao === 'mp4') {
      return 'application/mp4';

    } else {
      return undefined;
    }

  }

  getDiretorioDo(arquivo: Entry) {
    const caminhoDoDiretorio = arquivo.nativeURL.split('/');
    caminhoDoDiretorio.pop();
    return caminhoDoDiretorio.join('/');
  }


  listar() {
    return this.file.listDir(this.file.dataDirectory, '').then((dir) => {

    }).catch(err => {
      console.log('Directory doesn exist');
    });
  }

  anexar(diretorio, arquivo, flag) {
    return this.file.getFile(diretorio, arquivo, flag).then((dir) => {

    }).catch(err => {
      console.log('Directory doesn exist');
    });

  }
}
