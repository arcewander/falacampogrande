import { Injectable } from '@angular/core';
import { CallNumber } from '@ionic-native/call-number/ngx';

@Injectable({
  providedIn: 'root'
})
export class LigarService {

  constructor(private callNumber: CallNumber) { }

  ligar(contato: string) {
    this.callNumber.callNumber(contato, true)
      .then(res => console.log('Launched dialer!', res))
      .catch(err => console.log('Error launching dialer', err));
  }
}
