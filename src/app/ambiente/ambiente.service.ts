import { Injectable } from '@angular/core';
import { Ambiente } from '../model/Ambiente';
import { SQLiteObject } from '@ionic-native/sqlite/ngx';
import { DatabaseService } from '../database/database.service';

@Injectable({
  providedIn: 'root'
})
export class AmbienteService {

  ambientes: Array<Ambiente> = new Array<Ambiente>();
  ambiente: Ambiente = new Ambiente();
  constructor(private databaseService: DatabaseService) {
    this.ambientes = new Array<Ambiente>();
  }

  listar(callback) {
    const sql = 'SELECT * FROM ambiente';
    const data: any = [];
    this.ambientes = new Array<Ambiente>();
    return this.databaseService.abrirConexao().then((db: SQLiteObject) => {

       return db.executeSql(sql, data).then((ambientes) => {
         if (ambientes.rows.length > 0) {
          for (let i = 0; i < ambientes.rows.length; i++) {
            let ambiente: Ambiente = new Ambiente();
            ambiente = ambientes.rows.item(i);
            this.ambientes.push(ambiente);
          }
         }
         callback(this.ambientes);
      }).catch(error => { console.log(JSON.stringify(error))});

    }).catch((err) => {
      console.log(err);
    });
  }

  ambienteAtual(callback?) {

    return this.databaseService.abrirConexao()
      .then((db: SQLiteObject) => {
        const sql = 'SELECT * FROM ambiente WHERE ativo = ? ';
        const data = [1];
        return db.executeSql(sql, data).then((results) => {

          if (results.rows.length > 0) {
            this.setAmbiente(results.rows.item(0));
            callback(results.rows.item(0));
          }
        }).catch(error => { console.log(JSON.stringify(error)); });

    }).catch((err) => {
      console.log(err);
    });
  }

  setAmbiente(ambiente: Ambiente) {
    this.ambiente = ambiente;
  }

  getAmbiente() {
      return this.ambiente;
  }

  update(ambiente) {
    const sql = 'UPDATE ambiente SET ativo= ? WHERE id= ? ';
    this.databaseService.abrirConexao().then((db: SQLiteObject) => {
      db.executeSql(sql,[1, ambiente.id]).then((result) => {
        console.info('1º upd: ' + ambiente);
        this.ambiente = ambiente;
        this.desabilitaAmbiente(this.ambiente);
      });
      }).catch(error => {console.log(JSON.stringify(error)); });
  }

  desabilitaAmbiente(ambiente) {
    const sql = 'UPDATE ambiente SET ativo = ? WHERE id != ? ';

    this.databaseService.abrirConexao().then((db: SQLiteObject) => {
      console.info('2º upd: ' + ambiente);
      db.executeSql(sql, [0 , ambiente.id]).then((result) => {
        //this.ambiente = ambiente;
      });
      }).catch(error => { console.log(error); });
  }

  remove() {
    return this.databaseService.abrirConexao()
      .then((db: SQLiteObject) => {
        return db.executeSql('DELETE FROM ambiente')
          .catch((e) => console.error(e));
      }).catch((e) => console.error(e));
  }
}
