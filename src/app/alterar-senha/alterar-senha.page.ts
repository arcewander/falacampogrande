import { Component, OnInit } from '@angular/core';
import { Alerta } from '../model/Alerta';
import { AlertaService } from '../alerta/alerta.service';
import { AlterarSenhaService } from './alterar-senha.service';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-alterar-senha',
  templateUrl: './alterar-senha.page.html',
  styleUrls: ['./alterar-senha.page.scss'],
})
export class AlterarSenhaPage implements OnInit {

  alerta: Alerta;

  passwordTypeNS = 'password';
  passwordShownNS = false;

  passwordTypeSA = 'password';
  passwordShownSA = false;

  senhaAtual: string;
  novaSenha: string;

  constructor(private alertaService: AlertaService,
              public navCtrl: NavController,
              private alterarSenhaService: AlterarSenhaService) { }

  ngOnInit() {
  }

  public togglePasswordNS() {
    if (this.passwordShownNS) {
      this.passwordShownNS = false;
      this.passwordTypeNS = 'password';
    } else {
      this.passwordShownNS = true;
      this.passwordTypeNS = 'text';
    }
  }

  public togglePasswordSA() {
    if (this.passwordShownSA) {
      this.passwordShownSA = false;
      this.passwordTypeSA = 'password';
    } else {
      this.passwordShownSA = true;
      this.passwordTypeSA = 'text';
    }
  }

  async redefinirSenha() {
    const valid = this.validaForm();
    if (valid == true) {
      const result = this.alterarSenhaService.Alterar(this.senhaAtual, this.novaSenha)
        .subscribe(x => {
          if (x.mensagem == 'ERRO') {
            this.apresentarAlerta('Erro', 'Não foi possivel alterar sua senha.');
            return false;
          } else {
            this.apresentarAlerta('Sucesso', 'Senha alterada com sucesso.', () => {
              this.navCtrl.navigateForward('tela-inicial');
            });
            return false;
          }
        }, error => {
          this.apresentarAlerta('Erro', 'Não foi possivel alterar sua senha.');
          return false;
        }
        );
    }
  }


  validaForm() {
    if (this.senhaAtual == undefined || this.verificarCampoEmBranco(this.senhaAtual) == '') {
      this.apresentarAlerta('Atenção', 'Informe a senha atual.');
      return false;
    } else if (this.novaSenha == undefined || this.verificarCampoEmBranco(this.novaSenha) == '') {
      this.apresentarAlerta('Atenção', 'Informe a nova senha.');
      return false;
    } else if (this.senhaAtual == this.novaSenha) {
      this.apresentarAlerta('Atenção', 'As senhas não devem ser iguais.');
      return false;
    }
    return true;
  }

  verificarCampoEmBranco(input: string | string[]) {
    if (typeof input === 'string') {
      return input.trim();
    }
    else {
      return input;
    }
  }
  apresentarAlerta(titulo, mensagem, callback = () => { }) {
    this.alerta = new Alerta();
    this.alerta.titulo = titulo;
    this.alerta.mensagem = mensagem;
    this.alertaService.apresentarAlertaDeAviso(this.alerta, callback);
  }
}
