import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Alerta } from '../model/Alerta';
import { Ambiente } from '../model/Ambiente';
import { AlertaService } from '../alerta/alerta.service';
import { LoginService } from '../login/login.service';
import { AmbienteService } from '../ambiente/ambiente.service';


@Injectable({
  providedIn: 'root'
})
export class AlterarSenhaService {

  alerta: Alerta;

  ambiente: Ambiente = new Ambiente();

  constructor(private http: HttpClient,
              private alertaService: AlertaService,
              private loginService: LoginService,
              private ambienteService: AmbienteService) { }

  Alterar(senhaAtual: string, novaSenha: string) {

    this.ambiente = this.ambienteService.getAmbiente();
    const usuarioLogado = this.loginService.getUsuario();

    const json = {
      coduso: usuarioLogado.coduso,
      senuso: senhaAtual,
      novasenha: novaSenha
    };

    return this.http.post<AlterarSenhaResponse>(this.ambiente.url + 'acesso/alterasenha',
      json, {
        headers: {
          'Content-Type': 'application/json'
        }
      });
  }
}

export class AlterarSenhaResponse {
  mensagem: string;
}


