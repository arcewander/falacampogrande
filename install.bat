call ionic cordova plugin add cordova-plugin-app-version
call npm install @ionic-native/app-version

call ionic cordova plugin add com-badrit-base64
call npm install @ionic-native/base64

call ionic cordova plugin add call-number
call npm install @ionic-native/call-number

call ionic cordova plugin add cordova-plugin-camera
call  npm install @ionic-native/camera

call ionic cordova plugin add cordova-plugin-filechooser
call npm install @ionic-native/file-chooser

call ionic cordova plugin add cordova-plugin-filepath
call npm install @ionic-native/file-path

call ionic cordova plugin add cordova-plugin-file
call npm install @ionic-native/file

call ionic cordova plugin add cordova-plugin-geolocation
call npm install @ionic-native/geolocation

call ionic cordova plugin add cordova-plugin-inappbrowser
call npm install @ionic-native/in-app-browser

call ionic cordova plugin add cordova-sqlite-storage
call npm install @ionic-native/sqlite
